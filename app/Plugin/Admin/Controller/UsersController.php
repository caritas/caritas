<?php
	class UsersController extends AdminAppController {
		
		public $components = array('RequestHandler');
		
		public function login() {
			$this->layout = 'Admin.login';
			
			if ($this->request->is('post')) {
				$data = $this->request->data;
				
				if ($this->Auth->login()){
					$this->redirect('/admin');
				}
			}
		}
		
		public function logout() {
			$this->Auth->logout();
			$this->redirect('/admin');
		}
		
		public function profile() {
			$user = $this->Auth->user();
			$this->set('data', $user);
			$this->set('_serialize', array('data'));
			
		}
		
	}