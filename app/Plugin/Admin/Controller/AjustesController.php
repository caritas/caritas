<?php
	
	class AjustesController extends AdminAppController {
		
		public $uses = array('Admin.Chamada');
		
		public function filhas() {
			
			$this->layout = 'Admin.theme';
			
			$this->Chamada->Behaviors->attach('Containable');
			$this->Chamada->contain();
			
			$chamadas = $this->Chamada->find('all',
				array(
					'conditions' => array(
						'Chamada.chamada_id is not null'	
					),
					'fields' => array(
						'chamada_id',
						'count(chamada_id) as qtd'
					),
					'group' => array(
						'chamada_id'
					)
				)
			);
			$notfound = array();
			foreach ($chamadas as $chamada) {
				$cham = array(
					'id' => $chamada['Chamada']['chamada_id'],
					'chamada_count' => $chamada[0]['qtd']
				);
				if ($this->Chamada->read(array('id'),$cham['id'])) {
					$saved = $this->Chamada->save($cham);
					pr($saved);
				} else {
					array_push($notfound, $cham['id']);
				}
			}
			echo '<br>Chamadas filhas não encontradas<br>';
			pr($notfound);
			$this->render(false);
			
		}
		
	}