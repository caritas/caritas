<?php

class AtendentesFone extends SistemaAppModel {

	public $useTable = 'atendentes_fones';
	
	public $belongsTo = array(
		'TiposFone' => array(
			'className' => 'Sistema.TiposFone',
			'foreignKey' => 'tipo_fone_id'
		)
	);
	
}