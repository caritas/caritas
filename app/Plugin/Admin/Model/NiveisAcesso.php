<?php

class NiveisAcesso extends AdminAppModel {

	public $useTable = 'niveis_acessos';

	public $hasMany = array(
		'Permissao' => array(
			'className' => 'Admin.Permissao',
			'foreignKey' => 'nivel_acesso_id'
		)
	);

}
