<?php

class InstituicaoEndereco extends AdminAppModel {

	public $useTable = 'instituicoes_enderecos';

	public $belongsTo = array(
		'Instituicao' => array(
			'className' => 'Admin.Instituicao',
			'foreignKey' => 'instituicao_id'
		),
		'Cidade' => array(
			'className' => 'Admin.Cidade'
		)
	);

}