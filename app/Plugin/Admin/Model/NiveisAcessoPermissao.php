<?php

class NiveisAcessoPermissao extends SistemaAppModel {

  public $useTable = 'niveis_acessos_permissoes';

  public $belongsTo = array(
    'Permissao' => array(
      'className' => 'Sistema.Permissao',
      'foreignKey' => 'permissao_id'
    ),
    'NiveisAcesso' => array(
      'className' => 'Sistema.NiveisAcesso',
      'foreignKey' => 'nivel_acesso_id'
    )
  );

}
