<?php

class Atendente extends AdminAppModel {

	public $useTable = 'atendentes';
	public $order = array(
		'Atendente.nome' => 'ASC'
	);
	
	public $belongsTo = array(
		'Sexo' => array(
			'className' => 'Admin.Sexo',
			'foreignKey' => 'sexo_id'
		),
		'NiveisAcesso' => array(
			'className' => 'Admin.NiveisAcesso',
			'foreignKey' => 'nivel_acesso_id'
		)
	);
	
	public $hasMany = array(
		'AtendentesEndereco' => array(
			'className' => 'Admin.AtendentesEndereco'
		),
		'AtendentesFone' => array(
			'className' => 'Admin.AtendentesFone'
		)
	);
	
}