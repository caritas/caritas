<?php
class Permissao extends SistemaAppModel {

  public $useTable = 'permissoes';

  public $belongsTo = array(
    'NiveisAcesso' => array(
      'className' => 'NiveisAcesso',
      'foreignKey' => 'nivel_acesso_id'
    )

  );
  
}
