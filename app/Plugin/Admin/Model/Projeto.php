<?php

class Projeto extends AdminAppModel {

	public $useTable = 'projetos';
	
	public $hasMany = array(
		'Chamada' => array(
			'className' => 'Admin.Chamada'
		)
	);

}