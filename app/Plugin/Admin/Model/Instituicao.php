<?php

class Instituicao extends AdminAppModel {

	public $useTable = 'instituicoes';
	
	public $order = array(
		'Instituicao.nome_fantasia' => 'ASC'
	);
	
	public $hasMany = array(
		'ContatosInstituicao' => array(
			'className' => 'Admin.ContatosInstituicao'
		),
		'Chamada' => array(
			'className' => 'Admin.Chamada'
		),
		'InstituicaoEndereco' => array(
			'className' => 'Admin.InstituicaoEndereco',
			'foreignKey' => 'instituicao_id'
		)
	);
	
	public $belongsTo = array(
		'TiposInstituicao' => array(
			'className' => 'Admin.TiposInstituicao',
			'foreignKey' => 'tipo_instituicao_id'
		)
	);

}