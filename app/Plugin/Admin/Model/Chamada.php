<?php

class Chamada extends AdminAppModel {
			
	public $hasMany = array(
		'Filhas' => array(
			'className' => 'Admin.Chamada',
			'foreignKey' => 'chamada_id'
		),
		'ChamadasProcedimento' => array(
			'className' => 'Admin.ChamadasProcedimento',
			'foreignKey' => 'chamada_id'
		)
	);

	public $belongsTo = array(

		'Pai' => array(
			'className' => 'Admin.Chamada',
			'foreignKey' => 'chamada_id',
			'counterCache' => true
		),
		'Instituicao' => array(
			'className' => 'Admin.Instituicao',
			'foreignKey' => 'instituicao_id'
		),
		'Contato' => array(
			'className' => 'Admin.Contato',
			'foreignKey' => 'contato_id'
		),
		'Fornecedor' => array(
			'className' => 'Admin.Fornecedor',
			'foreignKey' => 'fornecedor_id'
		),
		'Assunto' => array(
			'className' => 'Admin.Assunto',
			'foreignKey' => 'assunto_id'
		),
		'Atendente' => array(
			'className' => 'Admin.Atendente',
			'foreignKey' => 'atendente_id'
		),
		'Estado' => array(
			'className' => 'Admin.Estado',
			'foreignKey' => 'estado_id'
		),
		'Cidade' => array(
			'className' => 'Admin.Cidade',
			'foreignKey' => 'cidade_id'
		),
		'TiposChamada' => array(
			'className' => 'Admin.TiposChamada',
			'foreignKey' => 'tipo_chamada_id'
		),
		'Status' => array(
			'className' => 'Admin.Status',
			'foreignKey' => 'status_id'
		),
		'Prioridade' => array(
			'className' => 'Admin.Prioridade',
			'foreignKey' => 'prioridade_id'
		),
		'Projeto' => array(
			'className' => 'Admin.Projeto',
			'foreignKey' => 'projeto_id'
		),
		'Pedido' => array(
			'className' => 'Admin.Pedido',
			'foreignKey' => 'pedido_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
			foreach($results as $key => $value) {
				if ( isset($value['Chamada']['data_inicio']) ) {
					$results[$key]['Chamada']['data_inicio'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['Chamada']['data_inicio'] ) );
				}
				if ( isset($value['Chamada']['data_fim']) ) {
					$results[$key]['Chamada']['data_fim'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['Chamada']['data_fim'] ) );
				}
			}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['Chamada']['data_inicio']) ) {
			$this->data['Chamada']['data_inicio'] = gmdate('Y-m-d H:i:s', strtotime($this->data['Chamada']['data_inicio']));
		}
		return true;
	}
	
	

}