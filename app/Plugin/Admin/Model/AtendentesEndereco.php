<?php

class AtendentesEndereco extends SistemaAppModel {

	public $useTable = 'atendentes_enderecos';
	
	public $belongsTo = array(
		'TiposEndereco' => array(
			'className' => 'Sistema.TiposEndereco',
			'foreignKey' => 'tipo_enderecos_id'
		),
		'Cidade' => array(
			'className' => 'Sistema.Cidade',
			'foreignKey' => 'cidade_id'
		)
	);
	
}