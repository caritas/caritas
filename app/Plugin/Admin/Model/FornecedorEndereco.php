<?php

class FornecedorEndereco extends AdminAppModel {

	public $useTable = 'fornecedores_enderecos';

	public $belongsTo = array(
		'Fornecedor' => array(
			'className' => 'Admin.Fornecedor',
			'foreignKey' => 'fornecedor_id'
		),
		'Cidade' => array(
			'className' => 'Admin.Cidade',
			'foreignKey' => 'cidade_id'
		)
	);

}