<?php

class AtasPreco extends AdminAppModel {

	public $useTable = 'ata_precos';
	
	public $belongsTo = array(
		'Edital' => array(
			'className' => 'Admin.Edital',
			'foreignKey' => 'edital_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['AtasPreco']['data']) ) {
				$results[$key]['AtasPreco']['data'] = date('Y-m-d\TH:i:s', strtotime( $value['AtasPreco']['data'] ) );
			}
		}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['AtasPreco']['data']) ) {
			$this->data['AtasPreco']['data'] = date_format(date_create_from_format('Y-m-d\TH:i:s', $this->data['AtasPreco']['data'] ), 'Y-m-d H:i:s' );
		}
		return true;
	}

}