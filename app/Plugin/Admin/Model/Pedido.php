<?php

class Pedido extends AdminAppModel {

	public $useTable = 'pedidos';
	
	public $order = array(
		'Pedido.data_inicio' => 'DESC'
	);
	
	public $belongsTo = array(
		'Instituicao' => array(
			'className' => 'Admin.Instituicao'
		),
		'Projeto' => array(
			'className' => 'Admin.Projeto'
		),
		'Distribuidor' => array(
			'className' => 'Admin.Distribuidor'
		),
		'Convenio' => array(
			'className' => 'Admin.Convenio'
		),
		'TiposPagamento' => array(
			'className' => 'Admin.TiposPagamento',
			'foreignKey' => 'tipo_pagamento_id'
		),
		'Edital' => array(
			'className' => 'Admin.Edital'
		),
		'AtasPreco' => array(
			'className' => 'Admin.AtasPreco',
			'foreignKey' => 'ata_preco_id'
		),
		'Status' => array(
			'className' => 'Admin.Status'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Pedido']['data_inicio']) ) {
				$results[$key]['Pedido']['data_inicio'] = date('d/m/Y', strtotime( $value['Pedido']['data_inicio'] ) );
			}
			if ( isset($value['Pedido']['data_fim']) ) {
				$results[$key]['Pedido']['data_fim'] = date('d/m/Y', strtotime( $value['Pedido']['data_fim'] ) );
			}
		}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['Pedido']['data_inicio']) ) {
			$this->data['Pedido']['data_inicio'] = date_format(date_create_from_format('d/m/Y', $this->data['Pedido']['data_inicio'] ), 'Y-m-d' );
		}
		if ( !empty($this->data['Pedido']['data_fim']) ) {
			$this->data['Pedido']['data_fim'] = date_format(date_create_from_format('d/m/Y', $this->data['Pedido']['data_fim'] ), 'Y-m-d' );
		}
		return true;
	}

}