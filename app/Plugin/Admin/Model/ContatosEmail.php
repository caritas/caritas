<?php

class ContatosEmail extends AdminAppModel {

	public $useTable = 'contatos_emails';

	public $belongsTo = array(
		'TiposEmail' => array(
			'className' => 'Admin.TiposEmail',
			'foreignKey' => 'tipo_email_id'
		)
	);

}