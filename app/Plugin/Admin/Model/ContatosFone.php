<?php

class ContatosFone extends AdminAppModel {

	public $useTable = 'contatos_fones';
	
	public $belongsTo = array(
		'TiposFone' => array(
			'className' => 'Admin.TiposFone',
			'foreignKey' => 'tipo_fone_id'
		)
	);

}