<?php

class Edital extends AdminAppModel {

	public $useTable = 'editais';
	
	public $belongsTo = array(
		'Orgao' => array(
			'className' => 'Admin.Orgao'
		),
		'Projeto' => array(
			'className' => 'Admin.Projeto'
		)
	);

}