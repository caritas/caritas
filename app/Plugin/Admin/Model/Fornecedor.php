<?php

class Fornecedor extends AdminAppModel {

	public $useTable = 'fornecedores';
	
	public $order = array(
		'Fornecedor.nome_fantasia' => 'ASC'
	);

	public $hasMany = array(
		'Chamada' => array(
			'className' => 'Admin.Chamada',
			'foreignKey' => 'fornecedor_id'
		),
		'FornecedorEndereco' => array(
			'className' => 'Admin.FornecedorEndereco',
			'foreignKey' => 'fornecedor_id'
		),
		'ContatosFornecedor' => array(
			'className' => 'Admin.ContatosFornecedor',
			'foreignKey' => 'fornecedor_id'
		)
	);

}