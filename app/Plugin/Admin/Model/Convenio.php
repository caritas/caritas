<?php

class Convenio extends AdminAppModel {

	public $useTable = 'convenios';
	
	public $belongsTo = array(
		'Instituicao' => array(
			'className' => 'Admin.Instituicao',
			'foreignKey' => 'instituicao_id'
		),
		'Edital' => array(
			'className' => 'Admin.Edital',
			'foreignKey' => 'edital_id'
		),
		'TiposConvenio' => array(
			'className' => 'Admin.TiposConvenio',
			'foreignKey' => 'tipo_convenio_id'
		),
		'Orgao' => array(
			'className' => 'Admin.Orgao',
			'foreignKey' => 'orgao_id'
		)

	);
	
	/*
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Convenio']['data_publicacao']) ) {
				$results[$key]['Convenio']['data_publicacao'] = date('d/m/Y', strtotime( $value['Convenio']['data_publicacao'] ) );
			}
		}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['Convenio']['data_publicacao']) ) {
			$this->data['Convenio']['data_publicacao'] = date_format(date_create_from_format('d/m/Y', $this->data['Convenio']['data_publicacao'] ), 'Y-m-d' );
		}
		return true;
	}
	*/
}