<?php

class ContatosEndereco extends AdminAppModel {

	public $useTable = 'contatos_enderecos';
	
	public $belongsTo = array(
		'TiposEndereco' => array(
			'className' => 'Admin.TiposEndereco',
			'foreignKey' => 'tipo_endereco_id'
		),
		'Cidade' => array(
			'className' => 'Admin.Cidade',
			'foreignKey' => 'cidade_id'
		)
	);
	
}