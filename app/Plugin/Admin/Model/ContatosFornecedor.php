<?php

class ContatosFornecedor extends AdminAppModel {

	public $useTable = 'contatos_fornecedores';

	public $belongsTo = array(
		'Contato' => array(
			'className' => 'Admin.Contato',
			'foreignKey' => 'contato_id'
		),
		'Fornecedor' => array(
			'className' => 'Admin.Fornecedor',
			'foreignKey' => 'fornecedor_id'
		),
		'Cargo' => array(
			'className' => 'Admin.Cargo',
			'foreignKey' => 'cargo_id'
		),
		'SituacoesContato' => array(
			'className' => 'Admin.SituacoesContato',
			'foreignKey' => 'situacao_contato_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['ContatosFornecedor']['data_inicio']) ) {
				$results[$key]['ContatosFornecedor']['data_inicio'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['ContatosFornecedor']['data_inicio'] ) );
			}
			if ( isset($value['ContatosFornecedor']['data_fim']) ) {
				$results[$key]['ContatosFornecedor']['data_fim'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['ContatosFornecedor']['data_fim'] ) );
			}
		}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['ContatosFornecedor']['data_inicio']) ) {
			$this->data['ContatosFornecedor']['data_inicio'] = date_format(date_create_from_format('Y-m-d\TH:i:s.u\Z', $this->data['ContatosFornecedor']['data_inicio'] ), 'Y-m-d' );
		}
		if ( !empty($this->data['ContatosFornecedor']['data_fim']) ) {
			$this->data['ContatosFornecedor']['data_fim'] = date_format(date_create_from_format('Y-m-d\TH:i:s.u\Z', $this->data['ContatosFornecedor']['data_fim'] ), 'Y-m-d' );
		}
		return true;
	}

}