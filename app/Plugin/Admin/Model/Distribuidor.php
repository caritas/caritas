<?php

class Distribuidor extends AdminAppModel {

	public $useTable = 'distribuidores';
	
	public $belongsTo = array(
		'Fornecedor' => array(
			'className' => 'Admin.Fornecedor'
		)
	);

}