<?php

class ContatosInstituicao extends AdminAppModel {

	public $useTable = 'contatos_instituicoes';

	public $belongsTo = array(
		'Contato' => array(
			'className' => 'Admin.Contato',
			'foreignKey' => 'contato_id'
		),
		'Instituicao' => array(
			'className' => 'Admin.Instituicao',
			'foreignKey' => 'instituicao_id'
		),
		'Cargo' => array(
			'className' => 'Admin.Cargo',
			'foreignKey' => 'cargo_id'
		),
		'SituacoesContato' => array(
			'className' => 'Admin.SituacoesContato',
			'foreignKey' => 'situacao_contato_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['ContatosInstituicao']['data_inicio']) ) {
				$results[$key]['ContatosInstituicao']['data_inicio'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['ContatosInstituicao']['data_inicio'] ) );
			}
			if ( isset($value['ContatosInstituicao']['data_fim']) ) {
				$results[$key]['ContatosInstituicao']['data_fim'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['ContatosInstituicao']['data_fim'] ) );
			}
		}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['ContatosInstituicao']['data_inicio']) ) {
			$this->data['ContatosInstituicao']['data_inicio'] = date_format(date_create_from_format('Y-m-d\TH:i:s.u\Z', $this->data['ContatosInstituicao']['data_inicio'] ), 'Y-m-d' );
		}
		if ( !empty($this->data['ContatosInstituicao']['data_fim']) ) {
			$this->data['ContatosInstituicao']['data_fim'] = date_format(date_create_from_format('Y-m-d\TH:i:s.u\Z', $this->data['ContatosInstituicao']['data_fim'] ), 'Y-m-d' );
		}
		return true;
	}

}