<?php

class Item extends AdminAppModel {

	public $useTable = 'itens';
	
	public $belongsTo = array(
		'AtasPreco' => array(
			'className' => 'Admin.AtasPreco',
			'foreignKey' => 'ata_preco_id'
		),
		'Fornecedor' => array(
			'className' => 'Admin.Fornecedor',
			'foreignKey' => 'fornecedor_id'
		)
	);

}