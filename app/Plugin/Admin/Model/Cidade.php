<?php

class Cidade extends AdminAppModel {

	public $useTable = 'cidades';
	
	public $belongsTo = array(
		'Estado' => array(
			'className' => 'Admin.Estado',
			'foreignKey' => 'estado_id'
		)
	);
	
	public function ligacao($string = '') {
		$string = str_replace(' De ',' de ', $string);
		$string = str_replace(' Da ',' da ', $string);
		$string = str_replace(' Das ',' das ', $string);
		$string = str_replace(' Do ',' do ', $string);
		$string = str_replace(' Dos ',' dos ', $string);
		return $string;
	}
	
	public function firstUCase($string = '') {
		return $this->ligacao(mb_ucfirst(strtolower($string)));
	}

	public function wordUCase($string = '') {
		return $this->ligacao( mb_convert_case($string, MB_CASE_TITLE, "UTF-8") );
	}
	
	public function beforeSave($options = array()) {
		$this->data['Cidade']['Prefeito'] = $this->wordUCase($this->data['Cidade']['Prefeito']);
		$this->data['Cidade']['nome'] = $this->wordUCase($this->data['Cidade']['nome']);
		
		return true;
	}

}