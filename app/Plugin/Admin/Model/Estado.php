<?php

class Estado extends AdminAppModel {

	public $useTable = 'estados';
		
	protected $_schema = array(
    'id' => array(
        'type' => 'string',
        'length' => 2
    ),
    'nome' => array(
        'type' => 'string',
        'length' => 30
    )
);
	
}