<?php

class ChamadasProcedimento extends AdminAppModel {

	public $useTable = 'chamadas_procedimentos';

	public $belongsTo = array(
		'Procedimento' => array(
			'className' => 'Admin.Procedimento',
			'foreignKey' => 'procedimento_id'
		),
		'Atendente' => array(
			'className' => 'Admin.Atendente',
			'foreignKey' => 'atendente_id'
		),
		'Chamada' => array(
			'className' => 'Admin.Chamada',
			'foreignKey' => 'procedimento_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
			foreach($results as $key => $value) {
				if ( isset($value['ChamadasProcedimento']['data']) ) {
					$results[$key]['ChamadasProcedimento']['data'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['ChamadasProcedimento']['data'] ) );
				}
			}
		}
		return $results;
	}

}
