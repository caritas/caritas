<?php

class Contato extends AdminAppModel {

	public $useTable = 'contatos';
	
	public $order = array(
		'Contato.nome'=>'ASC'
	);
	
	public $validate = array(
		'nome' => array(
			'rule' => array('minLength', 4),
			'message' => 'Deve contar mais de 4 caracteres.'
		),
		'data_nascimento' => array(
			'rule' => array('minLength', 10),
			'allowEmpty' => true,
			'message' => 'Deve ser uma data válida.'
		)
	);

	public $hasMany = array(
		'ContatosInstituicao' => array(
			'className' => 'Admin.ContatosInstituicao',
			'foreignKey' => 'contato_id'
		),
		'ContatosFornecedor' => array(
			'className' => 'Admin.ContatosFornecedor',
			'foreignKey' => 'contato_id'
		),
		'ContatosEndereco' => array(
			'className' => 'Admin.ContatosEndereco',
			'foreignKey' => 'contato_id'
		),
		'ContatosFone' => array(
			'className' => 'Admin.ContatosFone',
			'foreignKey' => 'contato_id'
		),
		'ContatosEmail' => array(
			'className' => 'Admin.ContatosEmail',
			'foreignKey' => 'contato_id'
		),
		'ContatosLotacao' => array(
			'className' => 'Admin.ContatosLotacao',
			'foreignKey' => 'contato_id'
		)
	);
	
	public $belongsTo = array(
		'Sexo' => array(
			'className' => 'Admin.Sexo',
			'foreignKey' => 'sexo_id'
		),
		'SituacoesContato' => array(
			'className' => 'Admin.SituacoesContato',
			'foreignKey' => 'situacao_contato_id'
		)
	);
	
	public function afterFind($results, $primary = false) {
		if (is_array( $results )) {
		foreach($results as $key => $value) {
			if ( isset($value['Contato']['data_nascimento']) ) {
				$results[$key]['Contato']['data_nascimento'] = date('Y-m-d\TH:i:s.000\Z', strtotime( $value['Contato']['data_nascimento'] ) );
			}
		}
		}
		return $results;
	}
	
	public function beforeSave( $options = array() ) {
		if ( !empty($this->data['Contato']['data_nascimento']) ) {
			$this->data['Contato']['data_nascimento'] = date_format(date_create_from_format('Y-m-d\TH:i:s.u\Z', $this->data['Contato']['data_nascimento'] ), 'Y-m-d' );
		}
		return true;
	}

}