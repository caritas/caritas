caritasApp.cC({
	name: 'contatoCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;
		
		this.Contato = this.$resource('/api/contato/:id.json');
		this.ContatoFone = this.$resource('/api/contatofone/:contatoId/:id.json');
		this.ContatoEmail = this.$resource('/api/contatoemail/:contatoId/:id.json');
		this.ContatoEndereco = this.$resource('/api/contatoendereco/:contatoId/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.busca = {
			nome: ''
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		pesquisar: function() {
			this._load();
		},
		zerar: function() {
			this.$.busca.nome = '';
			this._load();
		},
		_load: function() {
			this.$.data = [];
			this.Contato.get(
				{
					page: this.$.paginator.page,
					sort: this.$.sort,
					populate: 'ContatosInstituicao.Instituicao,ContatosFornecedor.Fornecedor,SituacoesContato',
					q: 'Contato.nome.lk.'+this.$.busca.nome
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) {
					this.$.norecords = true;
				} else {
					this.$.norecords = false;
				}
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		edit: function(item) {
			location.href = '#/contato/edit/'+item.Contato.id;
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Contato.delete({id: item.Contato.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'contatoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Contato = this.$resource('/api/contato/:id.json');
		this.Sexo = this.$resource('/api/sexo/:id.json');
		this.SituacaoContato = this.$resource('/api/situacaocontato/:id.json');
		this.$.related = {};

		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.Sexo.get().$promise.
			then(function(data){
				this.$.related.Sexos = data.data;
			}.bind(this));
			this.SituacaoContato.get().$promise.
			then(function(data){
				this.$.related.SituacaoContatos = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.Contato.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				
			};
		},
		save: function() {
			this.Contato.save(this.$.form.Contato).$promise.
			then(function(data){
				location.href = '#/contato/edit/'+data.data.Contato.id+'/telefone';
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/contato';
		}
	}
});

caritasApp.cC({
	name: 'contatoFoneCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Contato = this.$resource('/api/contato/:id.json');
		this.ContatoFone = this.$resource('/api/contatofone/:contatoId/:id.json');
		this.$.sort = 'fone';
		this.$.noRecords = false;
		this.$.contato_id = this.$routeParams.id;
		this.$.q = 'ContatosFone.contato_id.eq.'+this.$routeParams.id;
		this._load();
		
	},
	methods: {
		_load: function() {
			this.$.data = [];
			this.Contato.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
			this.ContatoFone.get(
				{
					sort: this.$.sort,
					q: this.$.q
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) this.$.noRecords = true;
				else this.$.noRecords = false;
				this.$.data = data.data;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.ContatoFone.delete({id: item.ContatosFone.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'contatoFoneFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.ContatoFone = this.$resource('/api/contatofone/:id.json');
		this.Contato = this.$resource('/api/contato/:id.json');
		this.TipoFone = this.$resource('/api/tipotelefone/:id.json');
		this.$.related = {};

		if (this.$routeParams.foneId) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.TipoFone.get().$promise.
			then(function(data){
				this.$.related.TipoFones = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.ContatoFone.get({ id: this.$routeParams.foneId }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				ContatosFone: {
					contato_id: this.$routeParams.id
				}
			};
		},
		save: function() {
			this.ContatoFone.save(this.$.form.ContatosFone).$promise.
			then(function(data){
				location.href = '#/contato/edit/'+this.$.form.ContatosFone.contato_id+'/telefone';
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/contato/edit/'+this.$routeParams.id+'/telefone';
		}
	}
});

caritasApp.cC({
	name: 'contatoEmailCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Contato = this.$resource('/api/contato/:id.json');
		this.ContatoEmail = this.$resource('/api/contatoemail/:contatoId/:id.json');
		this.$.sort = 'email';
		this.$.noRecords = false;
		this.$.contato_id = this.$routeParams.id;
		this.$.q = 'ContatosEmail.contato_id.eq.'+this.$routeParams.id;
		this._load();
		
	},
	methods: {
		_load: function() {
			this.$.data = [];
			this.Contato.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
			this.ContatoEmail.get(
				{
					sort: this.$.sort,
					q: this.$.q
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) this.$.noRecords = true;
				else this.$.noRecords = false;
				this.$.data = data.data;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.ContatoEmail.delete({id: item.ContatosEmail.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'contatoEmailFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.ContatoEmail = this.$resource('/api/contatoemail/:id.json');
		this.Contato = this.$resource('/api/contato/:id.json');
		this.TipoEmail = this.$resource('/api/tipoemail/:id.json');
		this.$.related = {};

		if (this.$routeParams.emailId) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.TipoEmail.get().$promise.
			then(function(data){
				this.$.related.TipoEmails = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.ContatoEmail.get({ id: this.$routeParams.emailId }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				ContatosEmail: {
					contato_id: this.$routeParams.id
				}
			};
		},
		save: function() {
			this.ContatoEmail.save(this.$.form.ContatosEmail).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/contato/edit/'+this.$routeParams.id+'/email';
		}
	}
});

caritasApp.cC({
	name: 'contatoEnderecoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Contato = this.$resource('/api/contato/:id.json');
		this.ContatoEndereco = this.$resource('/api/contatoendereco/:contatoId/:id.json');
		this.$.sort = 'endereco';
		this.$.noRecords = false;
		this.$.contato_id = this.$routeParams.id;
		this.$.q = 'ContatosEndereco.contato_id.eq.'+this.$routeParams.id;
		this._load();
		
	},
	methods: {
		_load: function() {
			this.$.data = [];
			this.Contato.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
			this.ContatoEndereco.get(
				{
					sort: this.$.sort,
					q: this.$.q
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) this.$.noRecords = true;
				else this.$.noRecords = false;
				this.$.data = data.data;
			}.bind(this));
			
		},
		edit: function(item) {
			location.href = '#/contato/edit/'+this.$routeParams.id+'/endereco/edit/'+item.ContatosEndereco.id;
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.ContatoEndereco.delete({id: item.ContatosEndereco.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'contatoEnderecoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.ContatoEndereco = this.$resource('/api/contatoendereco/:id.json');
		this.Estado = this.$resource('/api/estado/:id.json');
		this.Cidade = this.$resource('/api/cidade/:id.json');
		this.Contato = this.$resource('/api/contato/:id.json');
		this.TipoEndereco = this.$resource('/api/tipoendereco/:id.json');
		this.$.related = {};
		
		if (this.$routeParams.enderecoId) {
			this._edit();
		} else {
			this._add();
		}
	},
	watch: {
		'{object}form.ContatosEndereco.estado_id': '_loadCidade'
	},
	methods: {
		_related: function() {
			this.Contato.get(
				{
					id: this.$routeParams.id
				}
			).$promise.
			then(function(data){
				this.$.related.Contato = data.data.Contato;
			}.bind(this));
			
			this.TipoEndereco.get().$promise.
			then(function(data){
				this.$.related.TipoEnderecos = data.data;
			}.bind(this));
			
			this.Estado.get({
				limit: 50
			}).$promise.
			then(function(data){
				this.$.related.Estados = data.data;
			}.bind(this));
		},
		_loadCidade: function() {
			this.$.related.Cidades = [];
			this.Cidade.get(
				{
					limit: 100,
					q: 'Cidade.estado_id.eq.'+this.$.form.ContatosEndereco.estado_id
				}
			).$promise.
			then(function(data){
				this.$.related.Cidades = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.$.contato_id = this.$routeParams.id;
			this.ContatoEndereco.get({ id: this.$routeParams.enderecoId }).$promise.
			then(function(data){
				console.log(data);
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				ContatosEndereco: {
					contato_id: this.$routeParams.id
				}
			};
		},
		save: function() {
			this.ContatoEndereco.save(this.$.form.ContatosEndereco).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/contato/edit/'+this.$routeParams.id+'/endereco';
		}
	}
});
