caritasApp.cC({
	name: 'cidadeCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;
		
		this.Cidade = this.$resource('/api/cidade/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Cidade.get({page: this.$.paginator.page}).$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Cidade.delete({id: item.Cidade.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'cidadeFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Cidade = this.$resource('/api/cidade/:id.json');
		this.Estado = this.$resource('/api/estado/:id.json');
		this.$.related = {};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.Estado.get().$promise.
			then(function(data){
				this.$.related.Estados = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.Cidade.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				
			};
		},
		save: function() {
			this.Cidade.save(this.$.form.Cidade).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/cidade';
		}
	}
});
