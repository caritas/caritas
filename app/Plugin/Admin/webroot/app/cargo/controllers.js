caritasApp.cC({
	name: 'cargoCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;
		this.Cargo = this.$resource('/api/cargo/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Cargo.get({page: this.$.paginator.page}).$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Cargo.delete({id: item.Cargo.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'cargoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Cargo = this.$resource('/api/cargo/:id.json');
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_edit: function() {
			this.Cargo.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this.$.form = {
				
			};
		},
		save: function() {
			this.Cargo.save(this.$.form.Cargo).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/cargo';
		}
	}
});
