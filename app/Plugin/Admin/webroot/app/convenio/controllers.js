caritasApp.cC({
	name: 'convenioCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;
		
		this.Convenio = this.$resource('/api/convenio/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = '-data_publicacao';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Convenio.get({page: this.$.paginator.page}).$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Convenio.delete({id: item.Convenio.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'convenioFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Convenio = this.$resource('/api/convenio/:id.json');
		this.Edital = this.$resource('/api/edital/:id.json');
		this.Orgao = this.$resource('/api/orgao/:id.json');
		this.TipoConvenio = this.$resource('/api/tipoconvenio/:id.json');
		this.Instituicao = this.$resource('/api/instituicao/:id.json');

		this.$.related = {};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.Edital.get().$promise.
			then(function(data){
				this.$.related.Editais = data.data;
			}.bind(this));
			this.Orgao.get().$promise.
			then(function(data){
				this.$.related.Orgaos = data.data;
			}.bind(this));
			this.TipoConvenio.get().$promise.
			then(function(data){
				this.$.related.TipoConvenios = data.data;
			}.bind(this));
			this.Instituicao.get().$promise.
			then(function(data){
				this.$.related.Instituicoes = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.Convenio.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				
			};
		},
		save: function() {
			this.Convenio.save(this.$.form.Convenio).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/convenio';
		}
	}
});
