caritasApp.cC({
	name: 'tipoenderecoCtrl', 
	inject: ['$scope','$resource','dialogs'],
	init: function() {
		this.TipoEndereco = this.$resource('/api/tipoendereco/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.TipoEndereco.get().$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.TipoEndereco.delete({id: item.TiposEndereco.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'tipoenderecoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.TipoEndereco = this.$resource('/api/tipoendereco/:id.json');
		
		this.$.related = {};
		
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			
		},
		_edit: function() {
			this._related();
			this.TipoEndereco.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				
			};
		},
		save: function() {
			this.TipoEndereco.save(this.$.form.TiposEndereco).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/tipoendereco';
		}
	}
});
