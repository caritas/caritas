caritasApp.cC({
	name: 'itemCtrl', 
	inject: ['$scope','$resource','dialogs','$locale'],
	init: function() {	
		this.Item = this.$resource('/api/item/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Item.get(
				{
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Item.delete({id: item.Item.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'itemFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Item = this.$resource('/api/item/:id.json');
		this.Fornecedor = this.$resource('/api/fornecedor/:id.json');
		this.AtaPreco = this.$resource('/api/atapreco/:id.json');
		this.$.related = {};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.Fornecedor.get().$promise.
			then(function(data){
				this.$.related.Fornecedores = data.data;
			}.bind(this));
			this.AtaPreco.get().$promise.
			then(function(data){
				this.$.related.AtaPrecos = data.data;
			}.bind(this));
			
			
		},
		_edit: function() {
			this._related();
			this.Item.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				data.data.Item.valor = parseFloat(data.data.Item.valor);
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				
			};
		},
		save: function() {
			this.Item.save(this.$.form.Item).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/item';
		}
	}
});
