caritasApp.cC({
	name: 'fornecedorEnderecoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Fornecedor = this.$resource('/api/fornecedor/:id.json');
		this.FornecedorEndereco = this.$resource('/api/fornecedorendereco/:id.json');
		this.Fornecedor.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.Fornecedor = data.data;
			}.bind(this));
		this.$.norecords = false;
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.fornecedor_id = this.$routeParams.id;
		this.$.q = 'FornecedorEndereco.fornecedor_id.eq.'+this.$routeParams.id;
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			
			this.FornecedorEndereco.get(
				{
					page: this.$.paginator.page,
					limit: this.$.paginator.limit,
					sort: this.$.sort,
					q: this.$.q
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) {
					this.$.norecords = true;
				} else {
					this.$.norecords = false;
					this.$.data = data.data;
					this.$.paginator = data.paginator;
				}
			}.bind(this));
			
		},
		edit: function(item) {
			//console.log(item);
			location.href = '#/fornecedor/edit/'+item.Fornecedor.id+'/enderecos/edit/'+item.FornecedorEndereco.id;
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.FornecedorEndereco.delete({id: item.FornecedorEndereco.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'fornecedorEnderecoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.FornecedorEndereco = this.$resource('/api/fornecedorendereco/:id.json');
		this.Fornecedor = this.$resource('/api/fornecedor/:id.json');
		this.TipoEndereco = this.$resource('/api/tipoendereco/:id.json');
		this.Cidade = this.$resource('/api/cidade/:id.json');
		this.Correios = this.$resource('http://api.postmon.com.br/v1/cep/:cep');
		
		this.$.related = {};

		this.$.busca = {
			nome: ''
		};

		if (this.$routeParams.enderecoFornecedorId) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.TipoEndereco.get({
				limit: 100
			}).$promise.then(function(data){
				this.$.TiposEnderecos = data.data;
			}.bind(this));
		},
		buscaCidade: function() {
			this.Cidade.get(
				{
					q: 'Cidade.nome.lk.'+this.$.busca.nome,
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.Cidades = data.data;
			}.bind(this));
		},
		_add: function() {
			this._related();
			this.$.Form = {
				FornecedorEndereco: {
					fornecedor_id: this.$routeParams.id
				}
			};
		},
		buscaCEP: function() {
			this.Correios.get({cep: this.$.Form.FornecedorEndereco.cep}).$promise
			.then(function(data){
				this.$.Form.FornecedorEndereco.estado_id = data.estado;
				this.$.Form.FornecedorEndereco.bairro = data.bairro;
				this.$.Form.FornecedorEndereco.logradouro = data.logradouro;
				this.$.busca.nome = data.cidade;
				this.buscaCidade();
			}.bind(this));
		},
		_edit: function() {
			
			this._related();

			this.FornecedorEndereco.get(
				{
					id: this.$routeParams.enderecoFornecedorId
				}
			).$promise.then(function(data){
				this.$.Form = data.data;
				this.$.busca.nome = data.data.Cidade.nome;
				this.buscaCidade();
			}.bind(this));
		},
		save: function() {
			this.FornecedorEndereco.save(this.$.Form.FornecedorEndereco).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/fornecedor/edit/'+this.$routeParams.id+'/enderecos';
		}
	}
});
