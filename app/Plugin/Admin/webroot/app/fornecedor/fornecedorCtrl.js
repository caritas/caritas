caritasApp.cC({
	name: 'fornecedorCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;

		this.Fornecedor = this.$resource('/api/fornecedor/:id.json', {id: '@id'});
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.busca = {
			nome: ''
		};
		this.$.sort = 'nome_fantasia';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		pesquisar: function() {
			this._load();
		},
		zerar: function() {
			this.$.busca.nome = '';
			this._load();
		},
		_load: function() {
			//this.$.loading = true;
			//this.$.norecords = false;
				
			this.$.data = [];
			this.Fornecedor.get(
				{
					page: this.$.paginator.page,
					sort: this.$.sort,
					q: 'Fornecedor.nome_fantasia.lk.'+this.$.busca.nome,
					populate: 'FornecedorEndereco.Cidade'
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) {
					this.$.norecords = true;
				} else {
					this.$.norecords = false;
				}
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		edit: function(item) {
			location.href = '#/fornecedor/edit/'+item.Fornecedor.id
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Fornecedor.delete({id: item.Fornecedor.id}).$promise.
				then(function(data){
					if (data.error != undefined) {
						var error = this.dialogs.error(
							'Erro',
							data.error
						);
					} else {
						this._load();
					}
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'fornecedorFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Fornecedor = this.$resource('/api/fornecedor/:id.json');
		this.TipoFornecedor = this.$resource('/api/tipofornecedor/:id.json');
		this.$.related = {};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			/*this.TipoFornecedor.get().$promise.
			then(function(data){
				//this.$.related.TiposFornecedores = data.data;
			}.bind(this));*/
		},
		_edit: function() {
			this._related();
			this.$.isEdit = true;
			this.Fornecedor.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.isEdit = false;
			this.$.form = {
				
			};
		},
		save: function() {
			this.Fornecedor.save(this.$.form.Fornecedor).$promise.
			then(function(data){
				location.href = '#/fornecedor/edit/'+data.data.Fornecedor.id+'/contatos/add';
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/fornecedor';
		}
	}
});

