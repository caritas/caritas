caritasApp.cC({
	name: 'fornecedorContatoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Fornecedor = this.$resource('/api/fornecedor/:id.json');
		this.ContatoFornecedor = this.$resource('/api/contatofornecedor/:id.json');
		this.$.sort = 'data_inicio';
		this.$.noRecords = false;
		this.$.fornecedor_id = this.$routeParams.id;
		this.$.q = 'ContatosFornecedor.fornecedor_id.eq.'+this.$routeParams.id;
		this._load();
		
	},
	methods: {
		_load: function() {
			this.$.data = [];
			this.Fornecedor.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
			this.ContatoFornecedor.get(
				{
					sort: this.$.sort,
					q: this.$.q
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) this.$.noRecords = true;
				else this.$.noRecords = false;
				this.$.data = data.data;
			}.bind(this));
			
		},
		edit: function(item) {
			location.href = '#/fornecedor/edit/'+item.ContatosFornecedor.fornecedor_id+'/contatos/edit/'+item.ContatosFornecedor.id;
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.ContatoFornecedor.delete({id: item.ContatosFornecedor.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'fornecedorContatoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.ContatoFornecedor = this.$resource('/api/contatofornecedor/:id.json');
		this.Contato = this.$resource('/api/contato/:id.json');
		this.Fornecedor = this.$resource('/api/fornecedor/:id.json');
		this.SituacaoContato = this.$resource('/api/situacaocontato/:id.json');
		this.Cargo = this.$resource('/api/cargo/:id.json');
		
		this.$.related = {};

		this.$.busca = {
			nome: ''
		};

		if (this.$routeParams.contatoFornecedorId) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.SituacaoContato.get().$promise.
			then(function(data){
				this.$.related.SituacaoContatos = data.data;
			}.bind(this));
			this.Cargo.get(
				{
					limit: 200,
					sort: 'nome'
				}
			).$promise.
			then(function(data){
				this.$.related.Cargos = data.data;
			}.bind(this));
		},
		buscaContato: function() {
			this.Contato.get(
				{
					q: 'Contato.nome.lk.'+this.$.busca.nome,
					limit: 10
				}
			).$promise.
			then(function(data){
				this.$.related.Contatos = data.data;
			}.bind(this));
		},
		_add: function() {
			this._related();
			this.$.form = {
				ContatosFornecedor: {
					fornecedor_id: this.$routeParams.id
				}
			};
		},
		_edit: function() {
			
			this._related();

			this.ContatoFornecedor.get(
				{
					id: this.$routeParams.contatoFornecedorId
				}
			).$promise.then(function(data){
				this.$.form = data.data;
				this.Contato.get(
					{
						id: data.data.Contato.id
					}
				).$promise.
				then(function(contato){
					this.$.related.Contatos = [];
					this.$.related.Contatos.push(contato.data);
					
				}.bind(this));

			}.bind(this));
		},
		save: function() {
			this.ContatoFornecedor.save(this.$.form.ContatosFornecedor).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/fornecedor/edit/'+this.$routeParams.id+'/contatos';
		}
	}
});
