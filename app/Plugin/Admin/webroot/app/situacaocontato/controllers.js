caritasApp.cC({
	name: 'situacaocontatoCtrl', 
	inject: ['$scope','$resource','dialogs'],
	init: function() {
		this.SituacoesContato = this.$resource('/api/situacaocontato/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.SituacoesContato.get(
				{
					page: this.$.paginator.page,
					sort: this.$.sort
				}
			).$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.SituacoesContato.delete({id: item.SituacoesContato.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'situacaocontatoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.SituacoesContato = this.$resource('/api/situacaocontato/:id.json');
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_edit: function() {
			this.SituacoesContato.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this.$.form = {
				
			};
		},
		save: function() {
			this.SituacoesContato.save(this.$.form.SituacoesContato).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/situacaocontato';
		}
	}
});
