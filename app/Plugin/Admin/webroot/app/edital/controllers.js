caritasApp.cC({
	name: 'editalCtrl', 
	inject: ['$scope','$resource','dialogs'],
	init: function() {
		this.Edital = this.$resource('/api/edital/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Edital.get().$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Edital.delete({id: item.Edital.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'editalFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Edital = this.$resource('/api/edital/:id.json');
		this.Orgao = this.$resource('/api/orgao/:id.json');
		this.Projeto = this.$resource('/api/projeto/:id.json');
		this.$.related = {};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.Orgao.get().$promise.
			then(function(data){
				this.$.related.Orgaos = data.data;
			}.bind(this));
			this.Projeto.get().$promise.
			then(function(data){
				this.$.related.Projetos = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.Edital.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				
			};
		},
		save: function() {
			this.Edital.save(this.$.form.Edital).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/edital';
		}
	}
});
