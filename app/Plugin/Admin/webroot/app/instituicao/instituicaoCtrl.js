caritasApp.cC({
	name: 'instituicaoCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;

		this.Instituicao = this.$resource('/api/instituicao/:id.json', {id: '@id'});
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.busca = {
			nome: ''
		};
		this.$.sort = 'nome_fantasia';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		pesquisar: function() {
			this._load();
		},
		zerar: function() {
			this.$.busca.nome = '';
			this._load();
		},
		_load: function() {
			this.$.data = [];
			this.Instituicao.get(
				{
					page: this.$.paginator.page,
					sort: this.$.sort,
					q: 'Instituicao.nome_fantasia.lk.'+this.$.busca.nome,
					populate: 'InstituicaoEndereco.Cidade,TiposInstituicao'
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) {
					this.$.norecords = true;
				} else {
					this.$.norecords = false;
				}
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		edit: function(item) {
			location.href = '#/instituicao/edit/'+item.Instituicao.id
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Instituicao.delete({id: item.Instituicao.id}).$promise.
				then(function(data){
					if (data.error != undefined) {
						var error = this.dialogs.error(
							'Erro',
							data.error
						);
					} else {
						this._load();
					}
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'instituicaoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Instituicao = this.$resource('/api/instituicao/:id.json');
		this.TipoInstituicao = this.$resource('/api/tipoinstituicao/:id.json');
		this.$.related = {};
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.TipoInstituicao.get().$promise.
			then(function(data){
				this.$.related.TiposInstituicoes = data.data;
			}.bind(this));
		},
		_edit: function() {
			this._related();
			this.$.isEdit = true;
			this.Instituicao.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.isEdit = false;
			this.$.form = {
				
			};
		},
		save: function() {
			this.Instituicao.save(this.$.form.Instituicao).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		saveAddContato: function() {
			this.Instituicao.save(this.$.form.Instituicao).$promise.
			then(function(data){
				location.href = '#/instituicao/edit/'+data.data.Instituicao.id+'/contatos/add';
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/instituicao';
		}
	}
});
