caritasApp.cC({
	name: 'instituicaoEnderecoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Instituicao = this.$resource('/api/instituicao/:id.json');
		this.InstituicaoEndereco = this.$resource('/api/instituicaoendereco/:id.json');
		this.Instituicao.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.Instituicao = data.data;
			}.bind(this));
		this.$.norecords = false;
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.instituicao_id = this.$routeParams.id;
		this.$.q = 'InstituicaoEndereco.instituicao_id.eq.'+this.$routeParams.id;
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			
			this.InstituicaoEndereco.get(
				{
					page: this.$.paginator.page,
					limit: this.$.paginator.limit,
					sort: this.$.sort,
					q: this.$.q
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) {
					this.$.norecords = true;
				} else {
					this.$.norecords = false;
					this.$.data = data.data;
					this.$.paginator = data.paginator;
				}
			}.bind(this));
			
		},
		edit: function(item) {
			location.href = '#/instituicao/edit/'+item.Instituicao.id+'/enderecos/edit/'+item.InstituicaoEndereco.id;
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.InstituicaoEndereco.delete({id: item.InstituicaoEndereco.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'instituicaoEnderecoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.InstituicaoEndereco = this.$resource('/api/instituicaoendereco/:id.json');
		this.Instituicao = this.$resource('/api/instituicao/:id.json');
		this.TipoEndereco = this.$resource('/api/tipoendereco/:id.json');
		this.Cidade = this.$resource('/api/cidade/:id.json');
		this.Correios = this.$resource('http://api.postmon.com.br/v1/cep/:cep');
		
		this.$.related = {};

		this.$.busca = {
			nome: ''
		};

		if (this.$routeParams.enderecoInstituicaoId) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.TipoEndereco.get({
				limit: 100
			}).$promise.then(function(data){
				this.$.TiposEnderecos = data.data;
			}.bind(this));
		},
		buscaCidade: function() {
			this.Cidade.get(
				{
					q: 'Cidade.nome.lk.'+this.$.busca.nome,
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.Cidades = data.data;
			}.bind(this));
		},
		_add: function() {
			this._related();
			this.$.Form = {
				InstituicaoEndereco: {
					instituicao_id: this.$routeParams.id
				}
			};
		},
		buscaCEP: function() {
			this.Correios.get({cep: this.$.Form.InstituicaoEndereco.cep}).$promise
			.then(function(data){
				console.log(data);
				this.$.Form.InstituicaoEndereco.estado_id = data.estado;
				this.$.Form.InstituicaoEndereco.bairro = data.bairro;
				this.$.Form.InstituicaoEndereco.logradouro = data.logradouro;
				this.$.busca.nome = data.cidade;
				this.buscaCidade();
			}.bind(this));
		},
		_edit: function() {
			
			this._related();

			this.InstituicaoEndereco.get(
				{
					id: this.$routeParams.enderecoInstituicaoId
				}
			).$promise.then(function(data){
				this.$.Form = data.data;
				this.$.busca.nome = data.data.Cidade.nome;
				this.buscaCidade();
			}.bind(this));
		},
		save: function() {
			this.InstituicaoEndereco.save(this.$.Form.InstituicaoEndereco).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/instituicao/edit/'+this.$routeParams.id+'/enderecos';
		}
	}
});
