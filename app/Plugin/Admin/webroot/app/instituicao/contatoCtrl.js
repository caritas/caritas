caritasApp.cC({
	name: 'instituicaoContatoCtrl', 
	inject: ['$scope','$resource','dialogs','$routeParams'],
	init: function() {
		this.Instituicao = this.$resource('/api/instituicao/:id.json');
		this.ContatoInstituicao = this.$resource('/api/contatoinstituicao/:id.json');
		this.Instituicao.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.Instituicao = data.data;
			}.bind(this));
		this.$.norecords = false;
		this.$.sort = 'data_inicio';
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.instituicao_id = this.$routeParams.id;
		this.$.q = 'ContatosInstituicao.instituicao_id.eq.'+this.$routeParams.id;
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			
			this.ContatoInstituicao.get(
				{
					page: this.$.paginator.page,
					limit: this.$.paginator.limit,
					sort: this.$.sort,
					q: this.$.q
				}
			).$promise.
			then(function(data){
				if (data.data.length == 0) {
					this.$.norecords = true;
				} else {
					this.$.norecords = false;
					this.$.data = data.data;
					this.$.paginator = data.paginator;
				}
			}.bind(this));
			
		},
		edit: function(item) {
			location.href = '#/instituicao/edit/'+item.ContatosInstituicao.instituicao_id+'/contatos/edit/'+item.ContatosInstituicao.id;
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.ContatoInstituicao.delete({id: item.ContatosInstituicao.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'instituicaoContatoFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.ContatoInstituicao = this.$resource('/api/contatoinstituicao/:id.json');
		this.Contato = this.$resource('/api/contato/:id.json');
		this.Instituicao = this.$resource('/api/instituicao/:id.json');
		this.SituacaoContato = this.$resource('/api/situacaocontato/:id.json');
		this.Cargo = this.$resource('/api/cargo/:id.json');
		
		this.$.related = {};

		this.$.busca = {
			nome: ''
		};

		if (this.$routeParams.contatoInstituicaoId) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			this.SituacaoContato.get().$promise.
			then(function(data){
				this.$.related.SituacaoContatos = data.data;
			}.bind(this));
			this.Cargo.get(
				{
					limit: 200,
					sort: 'nome'
				}
			).$promise.
			then(function(data){
				this.$.related.Cargos = data.data;
			}.bind(this));
		},
		buscaContato: function() {
			this.Contato.get(
				{
					q: 'Contato.nome.lk.'+this.$.busca.nome,
					limit: 10
				}
			).$promise.
			then(function(data){
				this.$.related.Contatos = data.data;
			}.bind(this));
		},
		_add: function() {
			this._related();
			this.$.form = {
				ContatosInstituicao: {
					instituicao_id: this.$routeParams.id
				}
			};
		},
		_edit: function() {
			
			this._related();

			this.ContatoInstituicao.get(
				{
					id: this.$routeParams.contatoInstituicaoId
				}
			).$promise.then(function(data){
				this.$.form = data.data;
				this.Contato.get(
					{
						id: data.data.Contato.id
					}
				).$promise.
				then(function(contato){
					this.$.related.Contatos = [];
					this.$.related.Contatos.push(contato.data);
					
				}.bind(this));

			}.bind(this));
		},
		save: function() {
			this.ContatoInstituicao.save(this.$.form.ContatosInstituicao).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/instituicao/edit/'+this.$routeParams.id+'/contatos';
		}
	}
});
