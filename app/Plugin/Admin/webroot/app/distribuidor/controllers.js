caritasApp.cC({
	name: 'distribuidorCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;
		
		this.Distribuidor = this.$resource('/api/distribuidor/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Distribuidor.get().$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Distribuidor.delete({id: item.Distribuidor.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'distribuidorFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Distribuidor = this.$resource('/api/distribuidor/:id.json');
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_edit: function() {
			this.Distribuidor.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this.$.form = {
				
			};
		},
		save: function() {
			this.Distribuidor.save(this.$.form.Distribuidor).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/distribuidor';
		}
	}
});
