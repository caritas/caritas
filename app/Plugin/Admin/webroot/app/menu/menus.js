caritasApp.service('Menu', function(){
	this.menus = [
		{
			name: 'menu-top',
			position: 'pull-left',
			items: [
				{
					title: 'Menu',
					route: '',
					items: [
						{
							title: 'Chamadas',
							route: '#/chamada'
						},
						{
							title: 'Contatos',
							route: '#/contato'
						},
						{
							title: 'Fornecedores',
							route: '#/fornecedor'
						},
						{
							title: 'Instituições',
							route: '#/instituicao'
						}
					]
				},
				{
					title: 'Tabelas',
					route: '',
					items: [
						{
							title: 'Assuntos',
							route: '#/assunto'
						},
						{
							title: 'Atas de Preço',
							route: '#/atapreco'
						},
						{
							title: 'Atividades',
							route: '#/atividade'
						},
						{
							title: 'Cargos',
							route: '#/cargo'
						},
						{
							title: 'Cidades',
							route: '#/cidade'
						},
						{
							title: 'Convênios',
							route: '#/convenio'
						},
						{
							title: 'Distribuidores',
							route: '#/distribuidor'
						},
						{
							title: 'Editais',
							route: '#/edital'
						},
						{
							title: 'Estados',
							route: '#/estado'
						},
						{
							title: 'Etapas',
							route: '#/etapa'
						},
						{
							title: 'Itens',
							route: '#/item'
						},
						{
							title: 'Órgãos',
							route: '#/orgao'
						},
						{
							title: 'Prioridades',
							route: '#/prioridade'
						},
						{
							title: 'Procedimentos',
							route: '#/procedimento'
						},
						{
							title: 'Processos',
							route: '#/processo'
						},
						{
							title: 'Projetos',
							route: '#/projeto'
						},
						{
							title: 'Sexos',
							route: '#/sexo'
						},
						{
							title: 'Situações de Contato',
							route: '#/situacaocontato'
						},
						{
							title: 'Status',
							route: '#/status'
						}
					]
				},
				{
					title: 'Tipos',
					route: '',
					items: [
						{
							title: 'de Chamadas',
							route: '#/tipochamada'
						},
						{
							title: 'de Convênios',
							route: '#/tipoconvenio'
						},
						{
							title: 'de Documentos',
							route: '#/tipodocumento'
						},
						{
							title: 'de Emails',
							route: '#/tipoemail'
						},
						{
							title: 'de Endereços',
							route: '#/tipoendereco'
						},
						{
							title: 'de Instituições',
							route: '#/tipoinstituicao'
						},
						{
							title: 'de Pagamentos',
							route: '#/tipopagamento'
						},
						{
							title: 'de Telefones',
							route: '#/tipotelefone'
						}
					]
				}
			]
		},
		{
			name: 'menu-right',
			position: 'pull-right',
			items: [
				{
					title: 'Segurança',
					route: '',
					items: [
						{
							title: 'Logout',
							route: '/logout'
						},
						{
							title: 'Perfil',
							route: '#/perfil'
						},
						{
							title: 'Atendentes',
							route: '#/atendente'
						},
						{
							title: 'Grupos',
							route: '#/grupo'
						}
					]
				}
			]
		}
	];
});