caritasApp.cC({
	name: 'menuCtrl', 
	inject: ['$scope','Menu','$resource','Data'],
	init: function() {
		this.$.menus = this.Menu.menus;
		this.Tema = this.$resource('https://bootswatch.com/api/4.json');
		this.Atendente = this.$resource('/api/atendente/:id.json', {id: '@id'});
		this._load();
	},
	methods: {
		_load: function() {
			this.Tema.get().$promise.
			then(function(data){
				this.$.Temas = data.themes;
				this._findTheme();
			}.bind(this));
		},
		_findTheme: function() {
			foundTheme = 'Lumen';
			this.$.Temas.forEach(function(item){
				if (item.name == this.Data.profile.tema) {
					foundTheme = item;
				}
			}.bind(this));
			$('#BootswatchThemeStyle').load(foundTheme.cssCdn, function(){
				$('#BootswatchTheme').remove();
			});
		},
		changeTema: function(tema) {
			
			$('#BootswatchThemeStyle').load(tema.cssCdn, function(){
				$('#BootswatchTheme').remove();
			});
			
			var atendente = {
				tema: tema.name
			};
			
			this.Atendente.save({id: this.Data.profile.id},atendente);
			
			this.Data.profile.tema = tema.name;
			this.$.Profile.tema = tema.name;
		}
	}
	
});