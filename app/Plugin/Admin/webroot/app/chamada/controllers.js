caritasApp.cC({
	name: 'chamadaCtrl', 
	inject: ['$scope','$resource','dialogs','$modal','Data','$routeParams'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;

		this.Chamada = this.$resource('/api/chamada/:id.json');
		this.Assunto = this.$resource('/api/assunto/:id.json');
		this.Projeto = this.$resource('/api/projeto/:id.json');
						
		this.$.paginator = {
			page: 1,
			limit: 12
		};
		this.$.sort = ['-data_inicio'];
		
		if (this.$routeParams.id) {
			this.$.queryDefault = ['Chamada.chamada_id.eq.'+this.$routeParams.id];
			this.$.pageTitle = 'Chamadas Filhas';
			this.$.chamada_pai = this.$routeParams.id;
		} else {
			this.$.queryDefault = ['Chamada.chamada_id.nu'];
			this.$.pageTitle = 'Lista de Chamadas';
		}
		this.$.query = angular.copy(this.$.queryDefault);
		if (this.Data.filtro == undefined) {
			this.$.filtro = {
				finalizado: 1,
				filhas: 1
			};
		} else {
			this.$.filtro = this.Data.filtro;
		}
		this.$.viewType = 'panel'; // Panels View como default
		this.filtersApply();
		this._loadAssuntos();
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this.Data.paginator.Chamada.page = this.$.paginator.page;
				this._load();
			}
		},
		changeType: function(tipo) {
			this.$.viewType = tipo;
		},
		filtersApply: function() {
			this.$.query = angular.copy(this.$.queryDefault); // Reset Query
			this._filterProjeto(); // Adiciona filtro do Projeto
			this._filterInst(); // Adiciona filtro de Instituicao / Fornecedor
			this._filterForn(); // Adiciona filtro de Instituicao / Fornecedor
			this._filterAtendente(); // Adiciona filtro por Atendente
			this._filterAssunto(); // Adiciona filtro de Assunto
			this._filterFinalizado(); // Adiciona filtro de finalizado
			this._filterData(); // Adiciona filtro por data inicial
			this._filterFilhas(); // Adiciona filtro com/sem filhas
		},
		_filterFilhas: function() {
			if (this.$.filtro.filhas == 3) {
				var fFilhas = 'Chamada.chamada_count.eq.0';
				this.$.query.push(fFilhas);
			} else if (this.$.filtro.filhas == 2) {
				var fFilhas = 'Chamada.chamada_count.gt.0';
				this.$.query.push(fFilhas);
			}
		},
		_filterData: function() {
			if (this.$.filtro.DataIni) {
				txtDataIni = this.$.filtro.DataIni.getFullYear()+'-'+(this.$.filtro.DataIni.getMonth()+1)+'-'+this.$.filtro.DataIni.getDate();
				var fDataIni = 'Chamada.data_inicio.gt.'+txtDataIni;
				this.$.query.push(fDataIni);
			}
			if (this.$.filtro.DataFim) {
				txtDataFim = this.$.filtro.DataFim.getFullYear()+'-'+(this.$.filtro.DataFim.getMonth()+1)+'-'+this.$.filtro.DataFim.getDate();
				var fDataFim = 'Chamada.data_inicio.lt.'+txtDataFim;
				this.$.query.push(fDataFim);
			}
		},
		_filterFinalizado: function() {
			if (this.$.filtro.finalizado == 3) {
				var fFinalizado = 'Chamada.data_fim.nn';
				this.$.query.push(fFinalizado);
			} else if (this.$.filtro.finalizado == 2) {
				var fFinalizado = 'Chamada.data_fim.nu';
				this.$.query.push(fFinalizado);
			}
		},
		_filterProjeto: function() {
			if (this.$.filtro.projeto_id) {
				var fProjeto = 'Chamada.projeto_id.eq.'+this.$.filtro.projeto_id;
				this.$.query.push(fProjeto);
			}
		},
		_filterInst: function() {
			if (this.$.filtro.Inst) {
				var fInst = 'Instituicao.nome_fantasia.lk.'+this.$.filtro.Inst;
				this.$.query.push(fInst);
			}
		},
		_filterForn: function() {
			if (this.$.filtro.Forn) {
				var fForn = 'Fornecedor.nome_fantasia.lk.'+this.$.filtro.Forn;
				this.$.query.push(fForn);
			}
		},
		_filterAtendente: function() {
			if (this.$.filtro.atendente) {
				var fAtend = 'Atendente.nome.lk.'+this.$.filtro.atendente;
				this.$.query.push(fAtend);
			}
		},
		_filterAssunto: function() {
			if (this.$.filtro.assunto_id) {
				var fAssunto = 'Chamada.assunto_id.eq.'+this.$.filtro.assunto_id;
				this.$.query.push(fAssunto);
			}
		},
		_loadAssuntos: function() {
			this.Assunto.get({limit:100}).$promise.
			then(function(data){
				this.$.assuntosListing = data.data;
			}.bind(this));
			this.Projeto.get().$promise.
			then(function(data){
				this.$.projetosListing = data.data;
			}.bind(this));
		},
		pesquisar: function() {
			this.Data.filtro = this.$.filtro;
			this.$.paginator.page = 1;
			this._load();
		},
		zerarPesquisa: function() {
			this.$.filtro = {};
			this.Data.filtro = this.$.filtro;
			this.$.paginator.page = 1;
			this.$.filtersApply();
			this._load();
		},
		_load: function() {
			this.$.emptyData = false;
			this.$.data = [];
			this.Chamada.get(
				{
					page: this.Data.paginator.Chamada.page,
					limit: this.$.paginator.limit,
					sort: this.$.sort.toString(),
					q: this.$.query.toString(),
					populate: 'Projeto,Filhas,ChamadasProcedimento,Instituicao.InstituicaoEndereco.Cidade,Fornecedor.FornecedorEndereco.Cidade,Assunto,Atendente'
				}
			).$promise.
			then(function(data){
				
				if (data.data.length > 0) {
					this.$.data = data.data;
					this.$.paginator = data.paginator;
					this.$.emptyData = false;
				} else {
					this.$.emptyData = true;
				}
			}.bind(this));
		},
		openModal: function (item) {
			
			this.Data.chamada = item;

			var modalInstance = this.$modal.open({
				templateUrl: '/admin/app/chamada/finalizar.html',
				controller: 'finalizarChamadaCtrl',
				item: item,
				backdrop: true
			});
			
			modalInstance.result.then(function (data) {
				this.Chamada.save({id:data.chamada_id}, data).$promise
					.then(function(data){
					}.bind(this));
			}.bind(this), function(data){
				this._load();
			}.bind(this));
		},
		del: function(item) {
			var dlg = this.dialogs.confirm('Mensagem','Tem Certeza ?');
			dlg.result.then(function(btn){
				this.Chamada.delete({id: item.Chamada.id});
				this._load();
			}.bind(this));
		},
		filhas: function(chamada_id) {
			location.href = '#/chamada/'+chamada_id+'/filha';
		}
	}
});

caritasApp.cC({
	name: 'finalizarChamadaCtrl', 
	inject: ['$scope','$resource','$routeParams','$modalInstance','Data'],
	init: function() {
		this.Chamada = this.$resource('/api/chamada/:id.json', {id:'@id'});
		this.Status = this.$resource('/api/status/:id.json');
		
		this.$.finalizarForm = {
			data_fim: new Date()
		};
		this.Status.get().$promise.
		then(function(data){
			this.$.status = data.data;
		}.bind(this));
		
	},
	methods: {
		cancelar: function () {
			this.$modalInstance.dismiss('cancel');
		},
		finalizar: function() {
			this.Chamada.save(
				{
					id: this.Data.chamada.Chamada.id
				},
			this.$.finalizarForm).$promise.then(function(){
				this.cancelar();
			}.bind(this));
		}
	}
});


caritasApp.cC({
	name: 'chamadaFormCtrl', 
	inject: ['$scope','$resource','$routeParams','$filter','dialogs','$interval'],
	init: function() {
		this.Chamada = this.$resource('/api/chamada/:id.json');
		this.Assunto = this.$resource('/api/assunto/:id.json');
		this.Prioridade = this.$resource('/api/prioridade/:id.json');
		this.Contato = this.$resource('/api/contato/:id.json');
		this.Projeto = this.$resource('/api/projeto/:id.json');
		this.Status = this.$resource('/api/status/:id.json');
		this.TipoChamada = this.$resource('/api/tipochamada/:id.json');
		this.Fornecedor = this.$resource('/api/fornecedor/:id.json');
		this.Instituicao = this.$resource('/api/instituicao/:id.json');
		this.InstituicaoEndereco = this.$resource('/api/instituicaoendereco/:id.json');
		this.FornecedorEndereco = this.$resource('/api/fornecedorendereco/:id.json');
		this.ContatoInstituicao = this.$resource('/api/contatoinstituicao/:id.json');
		this.ContatoFornecedor = this.$resource('/api/contatofornecedor/:id.json');
		
		this.Cargo = this.$resource('/api/cargo/:id.json');
		this.SituacaoContato = this.$resource('/api/situacaocontato/:id.json');

		this.ContatoFone = this.$resource('/api/contatofone/:id.json', {id: '@id'});
		this.TipoFone = this.$resource('/api/tipotelefone/:id.json');
		this.ContatoEmail = this.$resource('/api/contatoemail/:id.json', {id: '@id'});
		this.TipoEmail = this.$resource('/api/tipoemail/:id.json');
		
		this.Sexo = this.$resource('/api/sexo/:id.json');
		
		
		this.$.contatoSetData = true;
		this.$.contatoSetNew = false;
		this.$.contatoSetFind = false;

		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
		
		if (this.$routeParams.chamada_pai) {
			this.$.pageTitle = 'Adicionar Chamada Filha';
			this.$.chamada_pai = this.$routeParams.chamada_pai;
			this.$.projeto_pai = '';
		} else {
			this.$.pageTitle = 'Adicionar Chamada';			
		}
	},
	methods: {
		newContactChamada: function() {
			this.$.contatoSetData = false;
			this.$.contatoSetNew = true;
			this.$.contatoSetFind = false;
			this.$.newContato = {};
		},
		addContactChamada: function() {
			this.$.contatoSetData = false;
			this.$.contatoSetNew = false;
			this.$.contatoSetFind = true;
			this.$.newContato = {};
		},
		contatoSetDataForm: function() {
			this.$.contatoSetData = true;
			this.$.contatoSetNew = false;
			this.$.contatoSetFind = false;
		},
		contatoSetSaveNew: function() {
			data = {};
			data.nome = this.$.newContato.nome;
			data.data_nascimento = this.$.newContato.data_nascimento;
			data.sexo_id = this.$.newContato.sexo_id;
			data.cpf = this.$.newContato.cpf;
			data.situacao_contato_id = this.$.newContato.situacao_contato_id;
			this.Contato.save(data).$promise.then(function(data){
				this.$.newContato.contato_id = data.data.Contato.id;
				this.contatoSetSaveFind();
			}.bind(this));
			
		},
		contatoSetSaveFind: function() {
			
			data = {};
			data.contato_id = this.$.newContato.contato_id;
			data.cargo_id = this.$.newContato.cargo_id;
			data.situacao_contato_id = this.$.newContato.situacao_contatocargo_id;
			data.data_inicio = this.$.newContato.data_inicio;
			data.data_fim = this.$.newContato.data_fim;
			if (this.$.form.Chamada.instituicao_id) {
				data.instituicao_id = this.$.form.Chamada.instituicao_id;
				this.ContatoInstituicao.save(data).$promise.then(function(){
					this._loadContatosInst(data.instituicao_id);
					this.contatoSetDataForm();
					this.changeContato();
				}.bind(this));
			} else {
				data.fornecedor_id = this.$.form.Chamada.fornecedor_id;
				this.ContatoFornecedor.save(data).$promise.then(function(){
					this._loadContatosForn(data.fornecedor_id);
					this.contatoSetDataForm();
					this.changeContato();
				}.bind(this));
			}
			
		},
		instSelect: function(item) {
			this.$.form.Chamada.instituicao_id = item.Instituicao.id;
			delete this.$.form.Chamada.fornecedor_id;
			delete this.$.form.Chamada.contato_id;
			delete this.$.form.Contato;
			
			this.Instituicao.get({ id: item.Instituicao.id }).$promise
			.then(function(data){
				this.$.form.Instituicao = data.data.Instituicao;
			}.bind(this));
			
			this._loadContatosInst(item.Instituicao.id);
			this._loadHistoricoInst(item.Instituicao.id);
		},

		fornSelect: function(item) {
			this.$.form.Chamada.fornecedor_id = item.Fornecedor.id;
			delete this.$.form.Chamada.instituicao_id;
			delete this.$.form.Chamada.contato_id;
			delete this.$.form.Contato;
			
			this.Fornecedor.get({ id: item.Fornecedor.id }).$promise
			.then(function(data){
				this.$.form.Fornecedor = data.data.Fornecedor;
			}.bind(this));
			
			this._loadContatosForn(item.Fornecedor.id);
			this._loadHistoricoForn(item.Fornecedor.id);
		},
		
		typeContato: function(text) {
			return this.Contato.get(
				{
					q: 'Contato.nome.lk.'+text,
				}
			).$promise.then(function(data){
				return data.data;
			}.bind(this));
		},
		
		fornSelectOld: function(item_id) {
			delete this.$.form.Chamada.instituicao_id;
			this.Fornecedor.get({ id: item_id }).$promise
			.then(function(data){
				this.$.form.Fornecedor = data.data.Fornecedor;
			}.bind(this));
			this._loadContatosForn(item_id);
			this._loadHistoricoForn(item_id);
		},
		_loadHistoricoInst: function(instituicao_id) {
			this.Chamada.get(
				{ 
					q: 'Chamada.instituicao_id.eq.'+instituicao_id,
					sort: '-data_inicio',
					limit: 10
				}
			).$promise
			.then(function(data){
				this.$.form.Historico = data.data;
			}.bind(this));
		},
		_loadHistoricoForn: function(fornecedor_id) {
			this.Chamada.get(
				{ 
					q: 'Chamada.fornecedor_id.eq.'+fornecedor_id,
					sort: '-data_inicio',
					limit: 10
				}
			).$promise
			.then(function(data){
				this.$.form.Historico = data.data;
			}.bind(this));
		},
		_loadContatosInst: function(instituicao_id) {
			this.$.related.ContatosInstituicoes = [];
			this.ContatoInstituicao.get(
				{
					q: 'ContatosInstituicao.instituicao_id.eq.'+instituicao_id,
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.ContatosInstForn = data.data;
			}.bind(this));
		},
		_loadContatosForn: function(fornecedor_id) {
			this.$.related.ContatosInstForn = [];
			this.ContatoFornecedor.get(
				{
					q: 'ContatosFornecedor.fornecedor_id.eq.'+fornecedor_id,
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.ContatosInstForn = data.data;
			}.bind(this));
		},
		contatoSelect: function(item_id) {
			this.Contato.get({ id: item_id }).$promise
			.then(function(data){
				this.$.form.Contato = data.data.Contato;
				this._contatoData(data.data.Contato.id);
			}.bind(this));
		},
		newContatoSelect: function(item) {
			this.$.newContato.contato_id = item.Contato.id;
		},
		changeContato: function() {
			delete this.$.form.Contato;
			delete this.$.form.Chamada.contato_id;
		},
		_edit: function() {
			this._related();

			this.Chamada.get({id: this.$routeParams.id}).$promise.
			then(function(data){
				this.$.form = data.data;
				this._contatoData(data.data.Chamada.contato_id);
				if (data.data.Chamada.instituicao_id) {
					this._loadContatosInst(data.data.Instituicao.id);
					this._loadHistoricoInst(data.data.Instituicao.id);
				} else {
					this._loadContatosForn(data.data.Fornecedor.id);
					this._loadHistoricoForn(data.data.Fornecedor.id);
				}
				
			}.bind(this));
		},
		_contatoData: function(contato_id) {
			this.ContatoFone.get(
			{
				q: 'ContatosFone.contato_id.eq.'+contato_id	
			}
			).$promise.then(function(data){
				this.$.ContatoFones = data.data;
			}.bind(this));

			this.ContatoEmail.get(
			{
				q: 'ContatosEmail.contato_id.eq.'+contato_id	
			}
			).$promise.then(function(data){
				this.$.ContatoEmails	 = data.data;
			}.bind(this));

			if (this.$.form.Chamada.instituicao_id) {

				this.ContatoInstituicao.get(
				{
					q: 'ContatosInstituicao.contato_id.eq.'+contato_id+'&ContatosInstituicao.instituicao_id.eq.'+this.$.form.Chamada.instituicao_id
				}
				).$promise.then(function(data){
					this.$.ContatoCargosInst = data.data;
				}.bind(this));

			} else {
				this.ContatoFornecedor.get(
				{
					q: 'ContatosFornecedor.contato_id.eq.'+contato_id+'&ContatosFornecedor.fornecedor_id.eq.'+this.$.form.Chamada.fornecedor_id
				}
				).$promise.then(function(data){
					this.$.ContatoCargosForn = data.data;
				}.bind(this));	
			}
		},
		_add: function() {
			this._related();
			this.$.form = {
				Chamada: {
					data_inicio: new Date(),
					instituicao_id: null,
					fornecedor_id: null
				}
			};
			if (this.$routeParams.chamada_pai) {
				this.Chamada.get({
					id: this.$routeParams.chamada_pai
				}).$promise
				.then(function(data){
					this.$.form.Chamada.chamada_id = this.$routeParams.chamada_pai;
					this.$.form.Chamada.instituicao_id = data.data.Chamada.instituicao_id;
					this.$.form.Chamada.fornecedor_id = data.data.Chamada.fornecedor_id;
					this.$.form.Chamada.projeto_id = data.data.Chamada.projeto_id;
					this.$.projeto_pai = data.data.Projeto.nome;
					this.Instituicao.get({
						id: data.data.Chamada.instituicao_id
					}).$promise
					.then(function(data){
						this.instSelect(data.data);
					}.bind(this));
				}.bind(this));
			}
			this.$.InstForn = 'Inst';
		},
		
		toInst: function() {
			delete this.$.form.Fornecedor;
			delete this.$.form.Chamada.fornecedor_id;
			this.$.InstForn = 'Inst';
		},
		toForn: function() {
			delete this.$.form.Instituicao;
			delete this.$.form.Chamada.instituicao_id;
			this.$.InstForn = 'Forn';
		},
		
		typeInst: function(text) {
			return this.InstituicaoEndereco.get(
				{
					q: 'Instituicao.nome_fantasia.lk.'+text,
					populate: 'Instituicao,Cidade',
					limit: 50
				}
			).$promise.then(function(data){
				return data.data;
			}.bind(this));
		},

		typeForn: function(text) {
			return this.FornecedorEndereco.get(
				{
					q: 'Fornecedor.nome_fantasia.lk.'+text,
					populate: 'Fornecedor,Cidade',
					limit: 50
				}
			).$promise.then(function(data){
				return data.data;
			}.bind(this));
		},
		
		_related: function() {
			this.$.related = {};
			
			this.TipoFone.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.TipoFones = data.data;
			}.bind(this));
			
			this.TipoEmail.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.TipoEmails = data.data;
			}.bind(this));
			
			this.Cargo.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.Cargos = data.data;
			}.bind(this));
			this.SituacaoContato.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.SituacoesContatos = data.data;
			}.bind(this));
			
			this.Assunto.get({
				limit: 100
			}).$promise.
			then(function(data){
				this.$.related.Assuntos = data.data;
			}.bind(this));

			this.Prioridade.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.Prioridades = data.data;
			}.bind(this));
			
			this.Projeto.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.Projetos = data.data;
			}.bind(this));
			
			this.Status.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.Status = data.data;
			}.bind(this));
			
			this.TipoChamada.get(
				{
					limit: 100
				}
			).$promise.
			then(function(data){
				this.$.related.TiposChamadas = data.data;
			}.bind(this));
			
			this.Fornecedor.get({
				limit: 100
			}).$promise.
			then(function(data){
				this.$.related.Fornecedores = data.data;
			}.bind(this));
			
			this.Sexo.get({
				limit: 100
			}).$promise.
			then(function(data){
				this.$.related.Sexos = data.data;
			}.bind(this));
			

		},
		saveFim: function() {
			this.$.form.Chamada.data_fim = new Date();
			this.$.save();
		},
		save: function() {
			if (this.$.form.Chamada.instituicao_id == 0 && this.$.form.Chamada.fornecedor_id == 0) {
				this.dialogs.info('É necessário selecionar ou uma Instituição ou um Fornecedor!');
				return false;
			}
			if (this.$.form.Chamada.instituicao_id == 0)
				delete(this.$.form.Chamada.instituicao_id);
			if (this.$.form.Chamada.fornecedor_id == 0)
				delete(this.$.form.Chamada.fornecedor_id);
			if (this.$.chamada_pai) {
				this.$.form.Chamada.chamada_id = this.$.chamada_pai;
			}
				
			this.Chamada.save(this.$.form.Chamada).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		addContatoFone: function() {
			this.$.insertedFone = {
				ContatosFone: {
					id: null,
					fone: '',
					tipo_fone_id: undefined,
				}
			};
			this.$.ContatoFones.push(this.$.insertedFone);
		},
		saveFone: function(data, item, contato_id) {
			data.contato_id = contato_id;
			data.id = item.ContatosFone.id;
			this.ContatoFone.save({id: item.ContatosFone.id}, data);
		},
		removeFone: function(index, item) {
			var dlg = this.dialogs.confirm('Mensagem','Tem Certeza ?');
			dlg.result.then(function(btn){
				this.$.ContatoFones.splice(index,1);
				this.ContatoFone.delete({id: item.ContatosFone.id});
			}.bind(this));
		},
		showTipoFone: function(item) {
			var selected = [];
			if(item.ContatosFone.tipo_fone_id) {
				selected = this.$filter('filter')(this.$.related.TipoFones, {$: item.ContatosFone.tipo_fone_id});
			}
			return selected.length ? selected[0].TiposFone.nome : 'Não Selecionado';
		},
		addContatoEmail: function() {
			this.$.insertedEmail = {
				ContatosEmail: {
					id: null,
					email: '',
					tipo_email_id: undefined,
				}
			};
			this.$.ContatoEmails.push(this.$.insertedEmail);
		},
		saveEmail: function(data, item, contato_id) {
			data.contato_id = contato_id;
			data.id = item.ContatosEmail.id;
			this.ContatoEmail.save({id: item.ContatosEmail.id}, data);
		},
		removeEmail: function(index, item) {
			var dlg = this.dialogs.confirm('Mensagem','Tem Certeza ?');
			dlg.result.then(function(btn){
				this.$.ContatoEmails.splice(index,1);
				this.ContatoEmail.delete({id: item.ContatosEmail.id});
			}.bind(this));
		},
		showTipoEmail: function(item) {
			var selected = [];
			if(item.ContatosEmail.tipo_email_id) {
				selected = this.$filter('filter')(this.$.related.TipoEmails, {$: item.ContatosEmail.tipo_email_id});
			}
			return selected.length ? selected[0].TiposEmail.nome : 'Não Selecionado';
		},
		showCargoInst: function(item) {
			var selected = [];
			if(item.ContatosInstituicao.cargo_id) {
				selected = this.$filter('filter')(this.$.related.Cargos, {$: item.ContatosInstituicao.cargo_id});
			}
			return selected.length ? selected[0].Cargo.nome : 'Não Selecionado';
		},
		showCargoForn: function(item) {
			var selected = [];
			if(item.ContatosFornecedor.cargo_id) {
				selected = this.$filter('filter')(this.$.related.Cargos, {$: item.ContatosFornecedor.cargo_id});
			}
			return selected.length ? selected[0].Cargo.nome : 'Não Selecionado';
		},
		showSituacaoContatoInst: function(item) {
			var selected = [];
			if(item.ContatosInstituicao.situacao_contato_id) {
				selected = this.$filter('filter')(this.$.related.SituacoesContatos, {$: item.ContatosInstituicao.situacao_contato_id});
			}
			return selected.length ? selected[0].SituacoesContato.nome : 'Não Selecionado';
		},
		showSituacaoContatoForn: function(item) {
			var selected = [];
			if(item.ContatosFornecedor.situacao_contato_id) {
				selected = this.$filter('filter')(this.$.related.SituacoesContatos, {$: item.ContatosFornecedor.situacao_contato_id});
			}
			return selected.length ? selected[0].SituacoesContato.nome : 'Não Selecionado';
		},
		removeCargoInst: function(index, item) {
			var dlg = this.dialogs.confirm('Mensagem','Tem Certeza ?');
			dlg.result.then(function(btn){
				this.$.ContatoCargosInst.splice(index,1);
				this.ContatoInstituicao.delete({id: item.ContatosInsituicao.id});
			}.bind(this));
		},
		removeCargoForn: function(index, item) {
			var dlg = this.dialogs.confirm('Mensagem','Tem Certeza ?');
			dlg.result.then(function(btn){
				this.$.ContatoCargosForn.splice(index,1);
				this.ContatoFornecedor.delete({id: item.ContatosFornecedor.id});
			}.bind(this));
		},
		saveCargoInst: function(data, item, contato_id, instituicao_id) {
			data.contato_id = contato_id;
			data.instituicao_id = instituicao_id;
			data.id = item.ContatosInstituicao.id;
			
			this.ContatoInstituicao.save({id: item.ContatosInstituicao.id}, data);
		},
		saveCargoForn: function(data, item, contato_id, fornecedor_id) {
			data.contato_id = contato_id;
			data.fornecedor_id = fornecedor_id;
			data.id = item.ContatosFornecedor.id;
			
			this.ContatoFornecedor.save({id: item.ContatosFornecedor.id}, data);
		},
		addCargoInst: function() {
			this.$.insertedCargo = {
				ContatosInstituicao: {
					id: null,
					data_inicio: null,
					data_fim: null,
					cargo_id: undefined,
					situacao_contato_id: undefined
				}
			};
			this.$.ContatoCargosInst.push(this.$.insertedCargo);
		},
		addCargoForn: function() {
			this.$.insertedCargo = {
				ContatosFornecedor: {
					id: null,
					data_inicio: null,
					data_fim: null,
					cargo_id: undefined,
					situacao_contato_id: undefined
				}
			};
			this.$.ContatoCargosForn.push(this.$.insertedCargo);
		},
		cancel: function() {
			if (this.$routeParams.chamada_pai) {
				location.href = '#/chamada/'+this.$routeParams.chamada_pai+'/filha';
			} else {
				location.href = '#/chamada';
			}
		}
	}
});

caritasApp.cC({
	name: 'chamadaProcedimentoCtrl', 
	inject: ['$scope','$resource','$routeParams','Data','dialogs'],
	init: function() {
		this.ChamadaProcedimento = this.$resource('/api/chamadaprocedimento/:id.json');
		this.Chamada = this.$resource('/api/chamada/:id.json');
		this.$.chamada_id = this.$routeParams.id;
		this._loadChamada();
		this._loadProcedimentos();
	},
	methods: {
		_loadChamada: function() {
			this.Chamada.get({
				id: this.$routeParams.id
			}).$promise
			.then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		_loadProcedimentos: function() {
			this.ChamadaProcedimento.get(
				{
					q: 'ChamadasProcedimento.chamada_id.eq.'+this.$routeParams.id
				}
			).$promise
			.then(function(data){
				if (data.data.length == 0) {
					this.$.noRecords = true;
				} else {
					this.$.noRecords = false;
				}
				this.$.data = data.data;
			}.bind(this));
		},
		del: function(item) {
			var dlg = this.dialogs.confirm('Mensagem','Tem Certeza ?');
			dlg.result.then(function(btn){
				this.ChamadaProcedimento.delete({id: item.ChamadasProcedimento.id});
				location.href = '#/chamada/'+this.$routeParams.id+'/procedimento';
			}.bind(this));
		}
	}
});

caritasApp.cC({
	name: 'chamadaProcedimentoFormCtrl', 
	inject: ['$scope','$resource','$routeParams','Data'],
	init: function() {
		this.ChamadaProcedimento = this.$resource('/api/chamadaprocedimento/:id.json');
		this.Procedimento = this.$resource('/api/procedimento/:id.json');
		this.Chamada = this.$resource('/api/chamada/:id.json');
		this.$.related = {};
		this._loadChamada();
		
		if (this.$routeParams.chamada_procedimento_id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		chooseProcedimento: function(item) {
			this.$.related.Procedimentos.forEach(function(proc){
				if (this.$.form.ChamadasProcedimento.procedimento_id == proc.Procedimento.id) {
					this.$.form.ChamadasProcedimento.procedimento = proc.Procedimento.descricao;
				}
			}.bind(this));
		},
		_loadChamada: function() {
			this.Chamada.get({
				id: this.$routeParams.id
			}).$promise
			.then(function(data){
				this.$.Chamada = data.data;
			}.bind(this));
		},
		_related: function() {
			this.Procedimento.get().$promise
			.then(function(data){
				this.$.related.Procedimentos = data.data;
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/chamada/'+this.$routeParams.id+'/procedimento';
		},
		_edit: function() {
			this._related();
			this.ChamadaProcedimento.get({
				id: this.$routeParams.chamada_procedimento_id
			}).$promise
			.then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		_add: function() {
			this._related();
			this.$.form = {
				ChamadasProcedimento: {
					atendente_id: this.Data.profile.id,
					chamada_id: this.$routeParams.id,
					data: new Date()
				}
			};
		},
		save: function() {
			
			this.ChamadaProcedimento.save(
				this.$.form.ChamadasProcedimento
			).$promise.then(function(data){
				this.cancel();
			}.bind(this));
		}
	}
});