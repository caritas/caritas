caritasApp.cC({
	name: 'etapaCtrl', 
	inject: ['$scope','$resource','dialogs'],
	init: function() {
		this.Etapa = this.$resource('/api/etapa/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Etapa.get(
				{
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Etapa.delete({id: item.Etapa.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'etapaFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Etapa = this.$resource('/api/etapa/:id.json');
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_edit: function() {
			this.Etapa.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this.$.form = {
				
			};
		},
		save: function() {
			this.Etapa.save(this.$.form.Etapa).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/etapa';
		}
	}
});
