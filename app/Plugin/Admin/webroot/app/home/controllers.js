caritasApp.cC({
	name: 'homeCtrl', 
	inject: ['$scope','$resource','dialogs','$locale','Data','$interval'],
	init: function() {
		this.Chamada = this.$resource('/api/chamada/:id.json');
		this.interval = this.$interval(
			function() {
				this._loadChamadasAbertasUsuario();
			}.bind(this), 
			1000
		);
	},
	methods: {
		
		_loadChamadasAbertasUsuario: function() {
			
			if (this.Data.profile.id) {
				this.Chamada.get({
					q: 'Chamada.atendente_id.eq.'+this.Data.profile.id+', Chamada.data_fim.nu',
					sort: 'data_inicio',
					limit: 500
				}).$promise
				.then(function(data){
					this.$.ChamadasAbertas = data.data;
				}.bind(this));
				
				this.$interval.cancel(this.interval);
			}
		}
		
	}
});
