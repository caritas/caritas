caritasApp.cC({
	name: 'atendenteCtrl', 
	inject: ['$scope','$resource','dialogs','Data'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;

		this.Atendente = this.$resource('/api/atendente/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.Atendente.get(
				{
					page: this.$.paginator.page
				}
			).$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.Atendente.delete({id: item.Atendente.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'atendenteFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.Atendente = this.$resource('/api/atendente/:id.json');
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_edit: function() {
			this.Atendente.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				data.data.Atendente.aniversario = new Date(data.data.Atendente.aniversario);
				this.$.form = data.data;
				console.log(data.data);
			}.bind(this));
		},
		
		_add: function() {
			this.$.form = {
				
			};
		},
		save: function() {
			this.Atendente.save(this.$.form.Atendente).$promise.
			then(function(data){
				console.log(data);
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/atendente';
		}
	}
});
