caritasApp.cC({
	name: 'tipotelefoneCtrl', 
	inject: ['$scope','$resource','dialogs'],
	init: function() {
		this.TipoTelefone = this.$resource('/api/tipotelefone/:id.json');
		this.$.paginator = {
			page: 1,
			limit: 10
		};
		this.$.sort = 'nome';
		this.$.q = '';
		this._load();
		
	},
	watch: {
		'{object}paginator.page': '_watchPage'
	},
	methods: {
		_watchPage: function(oldValue, newValue) {
			if (oldValue != newValue) {
				this._load();
			}
		},
		_load: function() {
			this.$.data = [];
			this.TipoTelefone.get().$promise.
			then(function(data){
				this.$.data = data.data;
				this.$.paginator = data.paginator;
			}.bind(this));
			
		},
		del: function(item) {
			var confirm = this.dialogs.confirm(
				'Confirmação',
				'Tem Certeza que deseja excluir este registro?',
				{
					size: 'sm',
					backdrop: false
				}
			);
			confirm.result.then(function(btn){
				this.TipoTelefone.delete({id: item.TiposFone.id}).$promise.
				then(function(data){
					this._load();
				}.bind(this));
			}.bind(this));
		}
	}

});

caritasApp.cC({
	name: 'tipotelefoneFormCtrl', 
	inject: ['$scope','$resource','$routeParams'],
	init: function() {
		this.TipoTelefone = this.$resource('/api/tipotelefone/:id.json');
		
		this.$.related = {};
		
		if (this.$routeParams.id) {
			this._edit();
		} else {
			this._add();
		}
	},
	methods: {
		_related: function() {
			
		},
		_edit: function() {
			this._related();
			this.TipoTelefone.get({ id: this.$routeParams.id }).$promise.
			then(function(data){
				this.$.form = data.data;
			}.bind(this));
		},
		
		_add: function() {
			this._related();
			this.$.form = {
				
			};
		},
		save: function() {
			this.TipoTelefone.save(this.$.form.TiposFone).$promise.
			then(function(data){
				this.cancel();
			}.bind(this));
		},
		cancel: function() {
			location.href = '#/tipotelefone';
		}
	}
});
