caritasApp.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		controller: 'homeCtrl', 
		templateUrl: '/admin/app/home/home.html'
	})
	.when('/atendente', {
		controller: 'atendenteCtrl', 
		templateUrl: '/admin/app/atendente/index.html'
	})
	.when('/atendente/add', {
		controller: 'atendenteFormCtrl', 
		templateUrl: '/admin/app/atendente/form.html'
	})
	.when('/atendente/edit/:id', {
		controller: 'atendenteFormCtrl', 
		templateUrl: '/admin/app/atendente/form.html'
	})
	.when('/atividade', {
		controller: 'atividadeCtrl', 
		templateUrl: '/admin/app/atividade/index.html'
	})
	.when('/atividade/add', {
		controller: 'atividadeFormCtrl', 
		templateUrl: '/admin/app/atividade/form.html'
	})
	.when('/atividade/edit/:id', {
		controller: 'atividadeFormCtrl', 
		templateUrl: '/admin/app/atividade/form.html'
	})
	.when('/assunto', {
		controller: 'assuntoCtrl', 
		templateUrl: '/admin/app/assunto/index.html'
	})
	.when('/assunto/add', {
		controller: 'assuntoFormCtrl', 
		templateUrl: '/admin/app/assunto/form.html'
	})
	.when('/assunto/edit/:id', {
		controller: 'assuntoFormCtrl', 
		templateUrl: '/admin/app/assunto/form.html'
	})
	.when('/atapreco', {
		controller: 'ataprecoCtrl', 
		templateUrl: '/admin/app/ata-preco/index.html'
	})
	.when('/atapreco/add', {
		controller: 'ataprecoFormCtrl', 
		templateUrl: '/admin/app/ata-preco/form.html'
	})
	.when('/atapreco/edit/:id', {
		controller: 'ataprecoFormCtrl', 
		templateUrl: '/admin/app/ata-preco/form.html'
	})
	.when('/chamada', {
		controller: 'chamadaCtrl', 
		templateUrl: '/admin/app/chamada/index.html'
	})
	.when('/chamada/add', {
		controller: 'chamadaFormCtrl', 
		templateUrl: '/admin/app/chamada/form.html'
	})
	.when('/chamada/add/:chamada_pai', {
		controller: 'chamadaFormCtrl', 
		templateUrl: '/admin/app/chamada/form.html'
	})
	.when('/chamada/edit/:id', {
		controller: 'chamadaFormCtrl', 
		templateUrl: '/admin/app/chamada/form.html'
	})
	.when('/chamada/:id/procedimento', {
		controller: 'chamadaProcedimentoCtrl', 
		templateUrl: '/admin/app/chamada/procedimento.html'
	})
	.when('/chamada/:id/procedimento/add', {
		controller: 'chamadaProcedimentoFormCtrl', 
		templateUrl: '/admin/app/chamada/procedimentoForm.html'
	})
	.when('/chamada/:id/procedimento/edit/:chamada_procedimento_id', {
		controller: 'chamadaProcedimentoFormCtrl', 
		templateUrl: '/admin/app/chamada/procedimentoForm.html'
	})
	.when('/chamada/:id/filha', {
		controller: 'chamadaCtrl', 
		templateUrl: '/admin/app/chamada/index.html'
	})
	.when('/cargo', {
		controller: 'cargoCtrl', 
		templateUrl: '/admin/app/cargo/index.html'
	})
	.when('/cargo/add', {
		controller: 'cargoFormCtrl', 
		templateUrl: '/admin/app/cargo/form.html'
	})
	.when('/cargo/edit/:id', {
		controller: 'cargoFormCtrl', 
		templateUrl: '/admin/app/cargo/form.html'
	})
	.when('/cidade', {
		controller: 'cidadeCtrl', 
		templateUrl: '/admin/app/cidade/index.html'
	})
	.when('/cidade/add', {
		controller: 'cidadeFormCtrl', 
		templateUrl: '/admin/app/cidade/form.html'
	})
	.when('/cidade/edit/:id', {
		controller: 'cidadeFormCtrl', 
		templateUrl: '/admin/app/cidade/form.html'
	})
	.when('/contato', {
		controller: 'contatoCtrl', 
		templateUrl: '/admin/app/contato/index.html'
	})
	.when('/contato/add', {
		controller: 'contatoFormCtrl', 
		templateUrl: '/admin/app/contato/form.html'
	})
	.when('/contato/edit/:id', {
		controller: 'contatoFormCtrl', 
		templateUrl: '/admin/app/contato/form.html'
	})
	.when('/contato/edit/:id/telefone', {
		controller: 'contatoFoneCtrl', 
		templateUrl: '/admin/app/contato/telefones.html'
	})
	.when('/contato/edit/:id/telefone/add', {
		controller: 'contatoFoneFormCtrl', 
		templateUrl: '/admin/app/contato/telefonesForm.html'
	})
	.when('/contato/edit/:id/telefone/edit/:foneId', {
		controller: 'contatoFoneFormCtrl', 
		templateUrl: '/admin/app/contato/telefonesForm.html'
	})
	.when('/contato/edit/:id/email', {
		controller: 'contatoEmailCtrl', 
		templateUrl: '/admin/app/contato/emails.html'
	})
	.when('/contato/edit/:id/email/add', {
		controller: 'contatoEmailFormCtrl', 
		templateUrl: '/admin/app/contato/emailsForm.html'
	})
	.when('/contato/edit/:id/email/edit/:emailId', {
		controller: 'contatoEmailFormCtrl', 
		templateUrl: '/admin/app/contato/emailsForm.html'
	})
	.when('/contato/edit/:id/endereco', {
		controller: 'contatoEnderecoCtrl', 
		templateUrl: '/admin/app/contato/enderecos.html'
	})
	.when('/contato/edit/:id/endereco/add', {
		controller: 'contatoEnderecoFormCtrl', 
		templateUrl: '/admin/app/contato/enderecosForm.html'
	})
	.when('/contato/edit/:id/endereco/edit/:enderecoId', {
		controller: 'contatoEnderecoFormCtrl', 
		templateUrl: '/admin/app/contato/enderecosForm.html'
	})
	.when('/convenio', {
		controller: 'convenioCtrl', 
		templateUrl: '/admin/app/convenio/index.html'
	})
	.when('/convenio/add', {
		controller: 'convenioFormCtrl', 
		templateUrl: '/admin/app/convenio/form.html'
	})
	.when('/convenio/edit/:id', {
		controller: 'convenioFormCtrl', 
		templateUrl: '/admin/app/convenio/form.html'
	})
	.when('/distribuidor', {
		controller: 'distribuidorCtrl', 
		templateUrl: '/admin/app/distribuidor/index.html'
	})
	.when('/distribuidor/add', {
		controller: 'distribuidorFormCtrl', 
		templateUrl: '/admin/app/distribuidor/form.html'
	})
	.when('/distribuidor/edit/:id', {
		controller: 'distribuidorFormCtrl', 
		templateUrl: '/admin/app/distribuidor/form.html'
	})
	.when('/edital', {
		controller: 'editalCtrl', 
		templateUrl: '/admin/app/edital/index.html'
	})
	.when('/edital/add', {
		controller: 'editalFormCtrl', 
		templateUrl: '/admin/app/edital/form.html'
	})
	.when('/edital/edit/:id', {
		controller: 'editalFormCtrl', 
		templateUrl: '/admin/app/edital/form.html'
	})
	.when('/estado', {
		controller: 'estadoCtrl', 
		templateUrl: '/admin/app/estado/index.html'
	})
	.when('/estado/add', {
		controller: 'estadoFormCtrl', 
		templateUrl: '/admin/app/estado/form.html'
	})
	.when('/estado/edit/:id', {
		controller: 'estadoFormCtrl', 
		templateUrl: '/admin/app/estado/form.html'
	})
	.when('/etapa', {
		controller: 'etapaCtrl', 
		templateUrl: '/admin/app/etapa/index.html'
	})
	.when('/etapa/add', {
		controller: 'etapaFormCtrl', 
		templateUrl: '/admin/app/etapa/form.html'
	})
	.when('/etapa/edit/:id', {
		controller: 'etapaFormCtrl', 
		templateUrl: '/admin/app/etapa/form.html'
	})
	.when('/fornecedor', {
		controller: 'fornecedorCtrl', 
		templateUrl: '/admin/app/fornecedor/index.html'
	})
	.when('/fornecedor/add', {
		controller: 'fornecedorFormCtrl', 
		templateUrl: '/admin/app/fornecedor/form.html'
	})
	.when('/fornecedor/edit/:id', {
		controller: 'fornecedorFormCtrl', 
		templateUrl: '/admin/app/fornecedor/form.html'
	})
	.when('/fornecedor/edit/:id/contatos', {
		controller: 'fornecedorContatoCtrl', 
		templateUrl: '/admin/app/fornecedor/contatos.html'
	})
	.when('/fornecedor/edit/:id/contatos/add', {
		controller: 'fornecedorContatoFormCtrl', 
		templateUrl: '/admin/app/fornecedor/contatosForm.html'
	})
	.when('/fornecedor/edit/:id/contatos/edit/:contatoFornecedorId', {
		controller: 'fornecedorContatoFormCtrl', 
		templateUrl: '/admin/app/fornecedor/contatosForm.html'
	})
	.when('/fornecedor/edit/:id/enderecos', {
		controller: 'fornecedorEnderecoCtrl', 
		templateUrl: '/admin/app/fornecedor/enderecos.html'
	})
	.when('/fornecedor/edit/:id/enderecos/add', {
		controller: 'fornecedorEnderecoFormCtrl', 
		templateUrl: '/admin/app/fornecedor/enderecosForm.html'
	})
	.when('/fornecedor/edit/:id/enderecos/edit/:enderecoFornecedorId', {
		controller: 'fornecedorEnderecoFormCtrl', 
		templateUrl: '/admin/app/fornecedor/enderecosForm.html'
	})
	.when('/instituicao', {
		controller: 'instituicaoCtrl', 
		templateUrl: '/admin/app/instituicao/index.html'
	})
	.when('/instituicao/add', {
		controller: 'instituicaoFormCtrl', 
		templateUrl: '/admin/app/instituicao/form.html'
	})
	.when('/instituicao/edit/:id', {
		controller: 'instituicaoFormCtrl', 
		templateUrl: '/admin/app/instituicao/form.html'
	})
	.when('/instituicao/edit/:id/contatos', {
		controller: 'instituicaoContatoCtrl', 
		templateUrl: '/admin/app/instituicao/contatos.html'
	})
	.when('/instituicao/edit/:id/contatos/add', {
		controller: 'instituicaoContatoFormCtrl', 
		templateUrl: '/admin/app/instituicao/contatosForm.html'
	})
	.when('/instituicao/edit/:id/contatos/edit/:contatoInstituicaoId', {
		controller: 'instituicaoContatoFormCtrl', 
		templateUrl: '/admin/app/instituicao/contatosForm.html'
	})
	.when('/instituicao/edit/:id/enderecos', {
		controller: 'instituicaoEnderecoCtrl', 
		templateUrl: '/admin/app/instituicao/enderecos.html'
	})
	.when('/instituicao/edit/:id/enderecos/add', {
		controller: 'instituicaoEnderecoFormCtrl', 
		templateUrl: '/admin/app/instituicao/enderecosForm.html'
	})
	.when('/instituicao/edit/:id/enderecos/edit/:enderecoInstituicaoId', {
		controller: 'instituicaoEnderecoFormCtrl', 
		templateUrl: '/admin/app/instituicao/enderecosForm.html'
	})
	.when('/item', {
		controller: 'itemCtrl', 
		templateUrl: '/admin/app/item/index.html'
	})
	.when('/item/add', {
		controller: 'itemFormCtrl', 
		templateUrl: '/admin/app/item/form.html'
	})
	.when('/item/edit/:id', {
		controller: 'itemFormCtrl', 
		templateUrl: '/admin/app/item/form.html'
	})
	.when('/orgao', {
		controller: 'orgaoCtrl', 
		templateUrl: '/admin/app/orgao/index.html'
	})
	.when('/orgao/add', {
		controller: 'orgaoFormCtrl', 
		templateUrl: '/admin/app/orgao/form.html'
	})
	.when('/orgao/edit/:id', {
		controller: 'orgaoFormCtrl', 
		templateUrl: '/admin/app/orgao/form.html'
	})
	.when('/prioridade', {
		controller: 'prioridadeCtrl', 
		templateUrl: '/admin/app/prioridade/index.html'
	})
	.when('/prioridade/add', {
		controller: 'prioridadeFormCtrl', 
		templateUrl: '/admin/app/prioridade/form.html'
	})
	.when('/prioridade/edit/:id', {
		controller: 'prioridadeFormCtrl', 
		templateUrl: '/admin/app/prioridade/form.html'
	})
	.when('/procedimento', {
		controller: 'procedimentoCtrl', 
		templateUrl: '/admin/app/procedimento/index.html'
	})
	.when('/procedimento/add', {
		controller: 'procedimentoFormCtrl', 
		templateUrl: '/admin/app/procedimento/form.html'
	})
	.when('/procedimento/edit/:id', {
		controller: 'procedimentoFormCtrl', 
		templateUrl: '/admin/app/procedimento/form.html'
	})
	.when('/processo', {
		controller: 'processoCtrl', 
		templateUrl: '/admin/app/processo/index.html'
	})
	.when('/processo/add', {
		controller: 'processoFormCtrl', 
		templateUrl: '/admin/app/processo/form.html'
	})
	.when('/processo/edit/:id', {
		controller: 'processoFormCtrl', 
		templateUrl: '/admin/app/processo/form.html'
	})
	.when('/projeto', {
		controller: 'projetoCtrl', 
		templateUrl: '/admin/app/projeto/index.html'
	})
	.when('/projeto/add/', {
		controller: 'projetoFormCtrl', 
		templateUrl: '/admin/app/projeto/form.html'
	})
	.when('/projeto/edit/:id', {
		controller: 'projetoFormCtrl', 
		templateUrl: '/admin/app/projeto/form.html'
	})
	.when('/sexo', {
		controller: 'sexoCtrl', 
		templateUrl: '/admin/app/sexo/index.html'
	})
	.when('/sexo/add', {
		controller: 'sexoFormCtrl', 
		templateUrl: '/admin/app/sexo/form.html'
	})
	.when('/sexo/edit/:id', {
		controller: 'sexoFormCtrl', 
		templateUrl: '/admin/app/sexo/form.html'
	})
	.when('/situacaocontato', {
		controller: 'situacaocontatoCtrl', 
		templateUrl: '/admin/app/situacaocontato/index.html'
	})
	.when('/situacaocontato/add', {
		controller: 'situacaocontatoFormCtrl', 
		templateUrl: '/admin/app/situacaocontato/form.html'
	})
	.when('/situacaocontato/edit/:id', {
		controller: 'situacaocontatoFormCtrl', 
		templateUrl: '/admin/app/situacaocontato/form.html'
	})
	.when('/status', {
		controller: 'statusCtrl', 
		templateUrl: '/admin/app/status/index.html'
	})
	.when('/status/add', {
		controller: 'statusFormCtrl', 
		templateUrl: '/admin/app/status/form.html'
	})
	.when('/status/edit/:id', {
		controller: 'statusFormCtrl', 
		templateUrl: '/admin/app/status/form.html'
	})
	.when('/tipochamada', {
		controller: 'tipochamadaCtrl', 
		templateUrl: '/admin/app/tipochamada/index.html'
	})
	.when('/tipochamada/add', {
		controller: 'tipochamadaFormCtrl', 
		templateUrl: '/admin/app/tipochamada/form.html'
	})
	.when('/tipochamada/edit/:id', {
		controller: 'tipochamadaFormCtrl', 
		templateUrl: '/admin/app/tipochamada/form.html'
	})
	.when('/tipoconvenio', {
		controller: 'tipoconvenioCtrl', 
		templateUrl: '/admin/app/tipoconvenio/index.html'
	})
	.when('/tipoconvenio/add', {
		controller: 'tipoconvenioFormCtrl', 
		templateUrl: '/admin/app/tipoconvenio/form.html'
	})
	.when('/tipoconvenio/edit/:id', {
		controller: 'tipoconvenioFormCtrl', 
		templateUrl: '/admin/app/tipoconvenio/form.html'
	})
	.when('/tipodocumento', {
		controller: 'tipodocumentoCtrl', 
		templateUrl: '/admin/app/tipodocumento/index.html'
	})
	.when('/tipodocumento/add', {
		controller: 'tipodocumentoFormCtrl', 
		templateUrl: '/admin/app/tipodocumento/form.html'
	})
	.when('/tipodocumento/edit/:id', {
		controller: 'tipodocumentoFormCtrl', 
		templateUrl: '/admin/app/tipodocumento/form.html'
	})
	.when('/tipoemail', {
		controller: 'tipoemailCtrl', 
		templateUrl: '/admin/app/tipoemail/index.html'
	})
	.when('/tipoemail/add', {
		controller: 'tipoemailFormCtrl', 
		templateUrl: '/admin/app/tipoemail/form.html'
	})
	.when('/tipoemail/edit/:id', {
		controller: 'tipoemailFormCtrl', 
		templateUrl: '/admin/app/tipoemail/form.html'
	})
	.when('/tipotelefone', {
		controller: 'tipotelefoneCtrl', 
		templateUrl: '/admin/app/tipotelefone/index.html'
	})
	.when('/tipotelefone/add', {
		controller: 'tipotelefoneFormCtrl', 
		templateUrl: '/admin/app/tipotelefone/form.html'
	})
	.when('/tipotelefone/edit/:id', {
		controller: 'tipotelefoneFormCtrl', 
		templateUrl: '/admin/app/tipotelefone/form.html'
	})
	.when('/tipoendereco', {
		controller: 'tipoenderecoCtrl', 
		templateUrl: '/admin/app/tipoendereco/index.html'
	})
	.when('/tipoendereco/add', {
		controller: 'tipoenderecoFormCtrl', 
		templateUrl: '/admin/app/tipoendereco/form.html'
	})
	.when('/tipoendereco/edit/:id', {
		controller: 'tipoenderecoFormCtrl', 
		templateUrl: '/admin/app/tipoendereco/form.html'
	})
	.when('/tipoinstituicao', {
		controller: 'tipoinstituicaoCtrl', 
		templateUrl: '/admin/app/tipoinstituicao/index.html'
	})
	.when('/tipoinstituicao/add', {
		controller: 'tipoinstituicaoFormCtrl', 
		templateUrl: '/admin/app/tipoinstituicao/form.html'
	})
	.when('/tipoinstituicao/edit/:id', {
		controller: 'tipoinstituicaoFormCtrl', 
		templateUrl: '/admin/app/tipoinstituicao/form.html'
	})
	.when('/tipopagamento', {
		controller: 'tipopagamentoCtrl', 
		templateUrl: '/admin/app/tipopagamento/index.html'
	})
	.when('/tipopagamento/add', {
		controller: 'tipopagamentoFormCtrl', 
		templateUrl: '/admin/app/tipopagamento/form.html'
	})
	.when('/tipopagamento/edit/:id', {
		controller: 'tipopagamentoFormCtrl', 
		templateUrl: '/admin/app/tipopagamento/form.html'
	})
	.otherwise({
		templateUrl: '/admin/app/errors/404.html'
	});
	
});