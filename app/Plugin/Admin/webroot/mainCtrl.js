caritasApp.cC({
	name: 'mainController', 
	inject: ['$scope','Menu','$resource','Data','$timeout'],
	init: function() {
		this.Data.userTimeout = this.Data.maxTimeout;
		
		this.$.version = '3.3.2';
		this.$.theme = 'lumen';
		this.Profile = this.$resource('/admin/users/profile.json');
		this._profile();
	},
	methods: {
		go: function(where) {
			location.href = where;
		},
		_profile: function() {
			if (!this.Data.profile.id) { 
				this.Profile.get().$promise
				.then(function(data){
					this.Data.profile = data.data;
					this.$.Profile = data.data;
				}.bind(this));
			}
			this.Data.userTimeout = this.Data.userTimeout - 1;
			this.$.timeTimeout = this.Data.userTimeout;
			
			if (this.Data.userTimeout == 0) {
				location.href='/logout';
			}
			this.$timeout(function() {
				this._profile();
			}.bind(this), 1*1000*60*10);
		}
	}
});
