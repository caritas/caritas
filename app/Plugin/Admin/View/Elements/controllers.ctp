<?php
	$controllers = array(
		'home',
		'assunto',
		'atendente',
		'chamada',
		'convenio',
		'ata-preco',
		'atividade',
		'chamada',
		'cargo',
		'cidade',
		'contato',
		'convenio',
		'distribuidor',
		'edital',
		'estado',
		'etapa',
		//'fornecedor', -> Carregamento especial logo abaixo
		//'instituicao', -> Carregamento especial logo abaixo
		'item',
		'orgao',
		'prioridade',
		'procedimento',
		'processo',
		'projeto',
		'sexo',
		'situacaocontato',
		'status',
		'tipochamada',
		'tipoconvenio',
		'tipodocumento',
		'tipoemail',
		'tipotelefone',
		'tipoendereco',
		'tipoinstituicao',
		'tipopagamento'
	);
	foreach ($controllers as $controller) {
		echo '<script src="/admin/app/'.$controller.'/controllers.js"></script>';
	}
	?>
	<script src="/admin/app/fornecedor/contatoCtrl.js"></script>
	<script src="/admin/app/fornecedor/fornecedorCtrl.js"></script>
	<script src="/admin/app/fornecedor/enderecoCtrl.js"></script>

	<script src="/admin/app/instituicao/contatoCtrl.js"></script>
	<script src="/admin/app/instituicao/instituicaoCtrl.js"></script>
	<script src="/admin/app/instituicao/enderecoCtrl.js"></script>
