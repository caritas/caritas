<!DOCTYPE html>

<html ng-app="caritasApp" ng-controller="mainController">
	<head>
		<meta charset="utf-8">
		<title>Cáritas</title>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<!-- Riot.js e Compiller.js -->
		<script src="https://cdn.jsdelivr.net/g/riot@2.0(riot.min.js+compiler.min.js)"></script>
		<style>
			.table td {
				vertical-align: middle !important;
				font-size: 15px !important;
			}
		</style>
		<!-- Tweeter Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		<style id="BootswatchThemeStyle">
		</style>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<!-- Angular Js -->
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.9/angular.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.2.15/angular-locale_pt-br.js"></script>
		
		<script src="http://code.angularjs.org/1.3.9/angular-route.js"></script>
		<script src="http://code.angularjs.org/1.3.9/angular-resource.js"></script>
		<script src="http://code.angularjs.org/1.3.9/angular-sanitize.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.0/ui-bootstrap-tpls.min.js"></script>
		
		<!-- Locais -->
		<script src="/admin/components/classy/angular-classy.min.js"></script>
		<script src="/admin/components/dialog/dialogs.min.js"></script>
		<script src="/admin/components/dialog/dialogs-default-translations.min.js"></script>
		<link href="/admin/components/dialog/dialogs.min.css" rel="stylesheet">
		
		<script src="/admin/components/angular-input-masks/angular-input-masks.min.js"></script>
		
		<script src="/admin/components/xeditable/js/xeditable.min.js"></script>
		<link href="/admin/components/xeditable/css/xeditable.css" rel="stylesheet">
		

		<script src="/admin/app.js"></script>
		<script src="/admin/Data.js"></script>
		<script src="/admin/mainCtrl.js"></script>
		<script src="/admin/routes.js"></script>
		<script src="/admin/components/dialog/config.js"></script>

		
		<!-- Menus -->
		<script src="/admin/app/menu/controllers.js"></script>
		<script src="/admin/app/menu/menus.js"></script>
		<!-- All Controllers -->
		<?php echo $this->Element('controllers'); ?>
		
	</head>
	<body>
		<div class="hidePage" ng-if="!hidePage">
		<?php echo $this->fetch('content'); ?>
		</div>
	</body>
</html>
