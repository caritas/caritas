<?php
class ContatoController extends ApiAppController {

	public $uses = array('Admin.Contato');
	
	public function delete($id = null) {
		$conditions = array(
			'contato_id' => $id
		);
		$this->Contato->ContatosFone->deleteAll($conditions);
		$this->Contato->ContatosEmail->deleteAll($conditions);
		$this->Contato->ContatosEndereco->deleteAll($conditions);

		$this->Contato->ContatosFornecedor->deleteAll($conditions);
		$this->Contato->ContatosInstituicao->deleteAll($conditions);
		
		$this->Contato->delete($id);

		$this->render(false);
	}
	
}

