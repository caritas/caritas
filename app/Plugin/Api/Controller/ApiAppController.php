<?php
class ApiAppController extends AppController {
		
	public $components = array(
		
			'RequestHandler',
			
			'Auth' => array(
				'loginAction' => array(
					'controller' => 'users',
					'action' => 'login',
					'plugin' => 'admin'
				),
				'authError' => 'Você não tem permissão de acesso!',
				'authenticate' => array(
					'Form' => array(
						'userModel' => 'Atendente',
						'fields' => array(
							'username' => 'email',
							'password'=>'senha'
						)
					)
				)
			)
			
		);
	
	public function _operators($oper) {
	
		$opers = array(
			'eq'=>'',
			'lt'=>'<',
			'gt'=>'>',
			'nq'=>'!=',
			'lk'=>'like',
			'nu'=>'IS NULL',
			'nn'=>'IS NOT NULL',
			'in'=>'',
			'ep'=>''
		);
		return $opers[$oper];
	}
	
	public function _query($wheres = '') {
		if ($wheres == '') return array();
		$result = array();
		$wheres = split(',', $wheres);
		
		foreach($wheres as $where) {
			
			$parts = split('\.', $where);
			if ($parts[2] == 'nu' || $parts[2] == 'nn') {
				array_push($result, 
					$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2])
				);
			} else if ($parts[2] == 'lk') {
				array_push($result, 
					array(
						$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2]) => '%'.$parts[3].'%'
					)
				);
			} else if ($parts[2] == 'in') {
				array_push($result, 
					array(
						$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2]) => explode('|', $parts[3])
					)
				);
			} else {
				array_push($result, 
					array(
						$parts[0].'.'.$parts[1].' '.$this->_operators($parts[2]) => $parts[3]
					)
				);
			}
		}

		return $result;
	}
	
	public function _sort($sorts = '') {
		$result = array();
		$sorts = split(',', $sorts);
		foreach($sorts as $sort) {
			$parts = split('-', $sort);
			if (count($parts) == 2) {
				array_push($result, 
					array(
						$this->modelClass.'.'.$parts[1] => 'DESC'
					)
				);
			} else {
				array_push($result, 
					array(
						$this->modelClass.'.'.$parts[0] => 'ASC'
					)
				);
			}
		}
		return $result;
	}

	public function index() {
				
		$query = $this->request->query;
		
		if (!isset($query['page'])) {
			$query['page'] = 1;
		}
		if (!isset($query['limit'])) {
			$query['limit'] = 10;
		}
		if (!isset($query['sort'])) {
			$query['sort'] = array();
		} else {
			$query['sort'] = $this->_sort($query['sort']);
		}
		if (!isset($query['q'])) {
			$query['q'] = '';
		} else {
			$query['q'] = $this->_query($query['q']);
		}
		if (!isset($query['fields'])) {
			$query['fields'] = '';
		}
				
		$offset = ($query['page'] - 1) * $query['limit'];
		
		$paginator = array(	
			'count' => $this->{$this->modelClass}->find(
				'count',
				array(
					'conditions'=>$query['q']
				)
			),
			'limit' => $query['limit'],
			'page' => $query['page']
		);

		if (isset($query['populate'])) {
			$this->{$this->modelClass}->Behaviors->attach('Containable');
			$this->{$this->modelClass}->contain(explode(',', $query['populate']));
		}
		
		$data = $this->{$this->modelClass}->find('all',
			array(
				'limit'=>$query['limit'],
				'offset'=>$offset,
				'order'=>$query['sort'],
				'conditions'=>$query['q']
			)
		);
		
		$this->set('paginator', $paginator);
		$this->set('data', $data);
		$this->set('_serialize', array( 'data', 'paginator' ) );
		
		$this->render(false);
		
	}
	
	public function view($id = null) {
		
		$data = $this->{$this->modelClass}->read(null,$id);
		$this->set('data', $data );
		$this->set('_serialize', array( 'data' ) );
		
		$this->render(false);
		
	}
	
	public function add() {
		
		$data = $this->request->input('json_decode');
		
		$this->{$this->modelClass}->save($data);
		$id = $this->{$this->modelClass}->id;
		$data = $this->{$this->modelClass}->read(null, $id);
		$this->set('data', $data );
		$this->set('_serialize', array( 'data' ) );
		
		$this->render(false);
		
	}
	
	public function edit($id = null) {
		
		$data = $this->request->data;
		$data['id'] = $id;
		
		$this->{$this->modelClass}->save($data);
				
		$this->set('data', $data );
		$this->set('_serialize', array( 'data' ) );
		$this->render(false);
		
	}
	
	public function delete($id = null) {
		try {
			$data = $this->{$this->modelClass}->delete($id);
		} catch (Exception $e) {
			echo json_encode( array('error'=>'Erro ao Excluir') );
		}
		$this->render(false);
		
	}
	
}
