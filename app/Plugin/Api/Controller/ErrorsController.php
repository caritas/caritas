<?php
	class ErrorsController extends ApiAppController {
		
		public function auth() {
			$this->layout = 'ajax';
			
			$error = array(
				'error' => 1,
				'message' => 'Não autorizado!'
			);
			echo json_encode($error);
			
			$this->render(false);
		}
		
	}