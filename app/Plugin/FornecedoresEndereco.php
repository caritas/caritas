<?php

class FornecedoresEndereco extends SistemaAppModel {

	public $useTable = 'fornecedores_enderecos';

	public $belongsTo = array(
		'Fornecedor' => array(
			'className' => 'Sistema.Fornecedor'
		),
		'Cidade' => array(
			'className' => 'Sistema.Cidade'
		)
	);

}